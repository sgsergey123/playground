function AppUtils()
{
	var self = this;
	this.timers = {};

	this.initEditableSelect = function(arg)
	{
		var selector = arg ? arg : '.editable-select';

		if (!$(selector+" input").val()) {
			$(selector+" input").val($(selector+" select").find('option:selected').html());
			$(this).trigger("chosen:updated");
			$(selector+" input").trigger('input');
		} else {
			$(selector+" select").val($(selector+" input").val());
		}

		$(selector+" select").change(function() {
			$(selector+" input").val($(this).find('option:selected').html());
			$(this).trigger("chosen:updated");
			$(selector+" input").trigger('input');
		});
	}

	// Find position of first occurrence of a string
	this.strpos =  function(haystack, needle, offset){
		var i = haystack.indexOf( needle, offset ); // returns -1
		return i >= 0 ? i : false;
	}

	this.in_array = function(needle, haystack, strict) {	// Checks if a value exists in an array
		//
		// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

		var found = false, key, strict = !!strict;

		for (key in haystack) {
			if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
				found = true;
				break;
			}
		}

		return found;
	}

	this.getTinyMceByClass = function(className)
	{
		var className = self.strpos(className, '.') === 0 ? className.substr(1) : className,
			finded = false;
		for (edId in tinymce.editors) {
			if (edId!=0 && tinymce.editors[edId].settings.selector == '.'+className) {
				finded = tinymce.editors[edId];
			}
		}
		return finded;
	}

	this.addQueryParams = function(url, params)
	{
		var spl = url.split('?');
		if ((typeof spl[1] == 'undefined') || (spl[1] == '')) {
			return spl[0]+'?'+$.param(params);
		} else {
			return url+'&'+$.param(params);
		}
	}

	this.getFingerprint = function()
	{
		if (typeof Fingerprint == 'function') {
			var fp = new Fingerprint({canvas: true, ie_activex: true, screen_resolution: true});
			return fp.get();
		} else {
			return null;
		}
	}

	this.addFingerPrint = function (idForm, name)
	{
		var element = document.getElementById(idForm);
		if (typeof element.tagName != 'undefined' && element.tagName.toLowerCase() == 'form') {
			$('#' + idForm).prepend($('<input>', {
				'type': 'hidden',
				'name': name,
				'value': self.getFingerprint()
			}));
		} else {
			$('#' + idForm).attr(name, self.getFingerprint());
		}

	};

	this.useFingerprint2 = function(callback)
	{
		if (typeof Fingerprint2=='function') {
			new Fingerprint2().get(function (result) {
				callback(result);
			});
		} else {
			return null;
		}
	}

	this.addFingerPrint2 = function (idForm, name)
	{
		self.useFingerprint2(function($result) {
			var element = document.getElementById(idForm);
			if (typeof element.tagName != 'undefined' && element.tagName.toLowerCase() == 'form') {
				$('#' + idForm).prepend($('<input>', {
					'type': 'hidden',
					'name': name,
					'value': $result
				}));
			} else {
				$('#' + idForm).attr(name, $result);
			}
		});
	};

	/**
	 * Метод заглушка
	 * @param data
	 */
	this.noop = function(data)
	{
	}

	this.formatMoney = function(a, c, d, t) {
		var n = a, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	/**
	 * Метод убирает дабл линки в атрибуте href и оставляет только одну ссылку.
	 * @param t string
	 * @returns string
	 */
	this.cleanExtraLinks = function(t)
	{
		var text = $('<div>',{html:t});
		var findedLinks = {};
		$(text).find('a').each(function() {
			findedLinks[$(this).attr('href')] = $(this).attr('href');
		});
		var firstLink = '';
		for (prop in findedLinks) {
			if (firstLink == '') {
				firstLink = prop;
			} else {
				$('a', text).each(function() {
					if ($(this).attr('href') == prop)
						$(this).replaceWith($(this).text());
				});
			}
		}
		return text.html();
	}

	this.scrollPage = function(pos, tuning, aMs)
	{
		var to = false, animateMs = aMs || 0;
		if (typeof pos == 'number') {
			to = pos;
		} else if (pos == 'top') {
			to = 0;
		} else if (pos == 'bottom') {
			to = $(document).height()+1000;
		} else if (pos.charAt(0) == '#' || pos.charAt(0) == '.' || pos.charAt(0) == '[') {
			if (typeof $(pos).offset() != 'undefined') {
				to = $(pos).offset().top;
			} else {
				to = false;
			}
			if (tuning) {
				to = to + parseFloat(tuning);
			}
		}
		if (to !== false) {
			$('html,body').animate({scrollTop: to}, animateMs);
		}
	}

	/**
	 * Метод перестраивает содержимое селектора
	 * @param target - целевой jquery selector тега <select>
	 * @param data - объект в котором ключ = value для тега <option>, значение = html для тега <option>
	 * @param selected - выбранное значение селектора. Если пустое, то выбранным будет первый элемент.
	 */
	this.rebuildTagSelect = function(target, data, selected)
	{
		$(target).empty();
		var selectedStr='';
		var selectedDetect=false;
		for (var key in data) {
			if (key == selected) {
				selectedStr = ' selected="selected"';
				selectedDetect = true;
			} else {
				selectedStr = '';
			}
			$(target).append('<option value="'+key+'"'+selectedStr+'>'+data[key]+'</option>');
		}
		if (typeof selected == 'undefined' || selected == '' || !selectedDetect) {
			$(target).find('option:last').attr("selected", "selected");
		}
		if (jQuery().chosen) {
			$(target).trigger("chosen:updated");
		}
		return null;
	}

	/**
	 * Установить курсор для jQuery элемента в требуемую позицию
	 * @param jqElem - jQuery объект. Например $('#element')
	 */
	this.setCursorPosition = function(jqElem, pos) {
		jqElem.each(function(index, elem) {
			if (elem.setSelectionRange) {
				elem.setSelectionRange(pos, pos);
			} else if (elem.createTextRange) {
				var range = elem.createTextRange();
				range.collapse(true);
				range.moveEnd('character', pos);
				range.moveStart('character', pos);
				range.select();
			}
		});
	}

	/**
	 * Обход (дамп) свойств объекты с алертом.
	 * @param obj
	 */
	this.var_dump = function(obj)
	{
		var d = '';
		dumpS = obj;
		for(var key in dumpS) {
			d += key+ " : "+dumpS[key]+"\n";
		}
		alert(d);
	};
	
	this.extend = function(first, second) {
		for (var prop in second){
			first[prop] = second[prop];
		}
	}

	this.getEventHandler = function(eventName, namespace)
	{
		var finded;
		if (typeof $._data(window, 'events')[eventName] != 'undefined') {
			var handlers = $._data(window, 'events')[eventName];
			for (prop in handlers) {
				if (handlers[prop] && handlers[prop].namespace == 'wloadmore') {
					return handlers[prop];
				}
			}
		}
		return false;
	};

	this.timerInit = function(options) {
		var now = new Date();
		var localDiff = typeof options.nowTime != 'undefined' ? options.nowTime - Math.round(now.getTime() / 1000) : 0;

		//alert(localDiff)
		var diff, opt = {
			doneCallback: typeof options.doneCallback == 'undefined' ? 'location.reload()' : options.doneCallback,
			format: options.format,
			expireTime: options.expireTime - localDiff,
		};
		if (options.startTime) {
			opt.startTime =  options.startTime - localDiff;
			diff = now.getTime()/1000 - opt.startTime;
		} else {
			diff = options.expireTime - now.getTime() / 1000;
		}

		self.timers[options.selector] = opt;

		$(options.selector).html(self.timerFormat(diff, options.format));

		self.timerRun();
	};

	this.timerRun = function()
	{
		var now = new Date();

		for (var selector in self.timers)
		{
			var data = self.timers[selector];
			var diff =  data.startTime ? Math.round(now.getTime()/1000) - data.startTime
				: data.expireTime - Math.round(now.getTime()/1000);
			$(selector).html(self.timerFormat(diff, data.format));

			if (data.expireTime) {
				diff = data.expireTime - Math.round(now.getTime()/1000);
				if (diff < 0) diff = 0;
				if (diff==0) {
					eval(data.doneCallback);
					return false;
				}
			}
		}

		setTimeout('appUtils.timerRun()', 1000);
	};

	this.timerFormat = function(t, format)
	{
		t = Math.round(t);
		var s;
		switch (format) {
			case 'seconds':
				s = t;
				break;
			default:
				var h;
				h = Math.floor(t/3600);
				h = h > 99 ? ('0'+h).slice(-3) : ('0'+h).slice(-2);
				s = h+':'+('0'+Math.floor(t/60)%60).slice(-2)+':'+('0' + t % 60).slice(-2);
		}
		return s;
	};

	this.timerReset = function()
	{
		appUtils.timers = {};
	};

	this.roundUpKrat = function(val, krat)
	{
		var x = val / krat;
		x = Math.ceil(x);
		return x * krat;
	};

	this.roundDownKrat = function(val, krat)
	{
		var x = val / krat;
		x = Math.floor(x);
		return x * krat;
	};
}
var appUtils = new AppUtils();