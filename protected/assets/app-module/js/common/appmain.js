function AppMain()
{
	this.appOptions = {
		idUser: null,
		hashUser: null,
		ratchetEnable: false,
		resourcesVersion: 0,
		isMatchMasksList: {},
		callbacks: []
	};

	this.regesteredWidgets = {};

	this.btnWidth;
	this.btnLabel;

	this.isFormHasError;

	this.defaultModalLayerContainer = '#modal_dialog_general';

	this.pageVisible = 1;

	this.historyStateData='';

	this.tinyMceConfig = {
		default: {
			theme: "modern",
			plugins: [
				"advlist autolink monuhus_link",
				"textcolor paste"
			],
			skin: 'monahus',
			toolbar: "bold italic underline | monuhus_link | fontselect | fontsizeselect | forecolor",
			fontsize_formats: "10px 12px 13px 14px 16px 18px 20px",
			font_formats : "Arial=arial,helvetica,sans-serif;"+
				"Courier New=courier new,courier;"+
				"Georgia=georgia,palatino;"+
				"Helvetica=helvetica;"+
				"Impact=impact,chicago;"+
				"Tahoma=tahoma,arial,helvetica,sans-serif;"+
				"Times New Roman=times new roman,times;"+
				"Verdana=verdana,geneva",
			relative_urls: false,
			remove_script_host: false,
			convert_urls : false,
			paste_preprocess : function(pl, o) {
				o.content = o.content.replace("<pre>", "");
				o.content = o.content.replace("</pre>", "");
			},
			invalid_elements : "table,form,input,textarea",
			setup : function(ed) {
				ed.on('change', function(e) {
					$('#'+ed.id).parents("form[data-init='dirty-check']").addClass('dirty');
				});
			},
			menubar:false,
			statusbar:false
			//content_css: "/media/css/tinymce-custom.css"
		}
	};

	this.redactorConfig = {
		'default': {
			buttons: [
				'formatting',
				'bold',
				'italic',
				'underline',
				'alignment',
				'image'
			],
			imageUploadParam: 'qqfile', //for compatibility with qqFileUploader widget
			fileUploadParam: 'qqfile',
			imageUploadErrorCallback: function (json, xhr) {
				jAlert(json.message);
			},
			dragFileUpload: false,
			minHeight: 300,
			imageEditable: false,
			autoresize: false,
			convertLinks: false,
			convertImageLinks: false
		}
	};

	this.scoreMessages = [];
	this.scoreMessagesInProgress = 0;
	this.layoutContainerSaved = {
		isSaved: false,
		selector: false,
		scrollTop: 0,
		windowScrollHandler: null
	};

	var self = this;

	this.initApp = function(options)
	{
		appUtils.extend(self.appOptions, options);

		// post request (live)
		$('body').on('click', '.ajax-post', function(event) {
			if ($(this).hasClass('ajax-post-stoppropogation')) {
				event.stopPropagation();
			}
			if ($(this).is('.in_progress_post') || $(this).is('.in_progress_request')) {
				return false;
			}
			self.ajaxPost(this);
			if (!$(this).hasClass('ajax-post-skippreventdefault')) {
				event.preventDefault();
			}
		});

		// get request (live)
		$('body').on('click', '.ajax-get', function(event) {
			if ($(this).is('.in_progress_request')) {
				return false;
			}

			// mark ajax menu item as active
			self.markMenuActive($(this));

			var scrollto = $(this).data('scrollto') == 'auto' ? $('html,body').scrollTop() : $(this).data('scrollto');

			self.ajaxLoadUrlHistory(self.getUrl(this), scrollto, event);

			if ($(this).hasClass('ajax-get-stoppropogation')) {
				event.stopPropagation();
			}
			event.preventDefault();
		});

		// get request (live)
		$('body').on('click', '.ajax-get-modal', function(event) {
			if ($(this).is('.in_progress_request')) {
				return false;
			}
            self.ajaxLoadUrlModal(self.getUrl(this), $(this).data('scrollto') || 'undefined', event);
			event.preventDefault();
			event.stopPropagation();
			return false;
		});

		$('body').on('click', '.stopPropagation', function(event) {
			event.stopPropagation();
		});

		$('body').on('click', '[data-depend-menu-item]', function(event) {
			self.markMenuActiveByName($(this).attr('data-depend-menu-item'));
		});

		$('body').popover({
			selector: '.live-hover-hint',
			html: true,
			trigger: 'hover'
		});

		// if history change - load page
		if (window.History.enabled) {
			$(window).bind('statechange', function() {

				if ($(window).data('cnf')) {
					$(window).data('cnf', false);
					return;
				}

				var execute=function() {
					var State = History.getState();
					var res =  /^.{4,5}?\:\/\/.+?(\/.+)/.exec(decodeURIComponent(State.url));
					var url = res ? res[1] : decodeURIComponent(State.url);
					self.ajaxLoadUrl(url, State.data.scrollto, {
						headers: {
							'Is-Modal-Request': 0,
							'Current-Layout': self.appOptions.currentLayout,
							'Resources-Version': self.appOptions.resourcesVersion,
							'Is-Match-Masks-List': JSON.stringify(self.appOptions.isMatchMasksList)
						}
					});
					self.appOptions.currentUrl = url;
					self.historyStateData = State.data;
				}

				if (self.isFormDirty()) {
					jConfirm('Your data has not been saved. Are you sure you want to leave this page?', 'Please confirm this action', function(cResult) {
						if (cResult) {
							$('form').removeClass('dirty');
							execute();
						} else {
							$(window).data('cnf', true);
							History.replaceState(self.historyStateData, document.title, self.appOptions.currentUrl);
						}
					});
				} else {
					execute();
				}

			});
		};

		// init modal
		self.initModal();

		// инициализация которая выполняется если страница перегружается по F5
		self.initGlobalPlugins();

		/**/
		$.alerts.overlayColor = '#000';
		$.alerts.overlayOpacity = 0.3;
		$.alerts.draggable = true;

		// Prevent bootstrap dialog from blocking focusin
		// Исправление баги tinymce+bootstrap modal: если у слоя modal прописан tabindex="-1",
		// то открыв второе модальное окна tinymce, например "вставка ссылки", мы не можем установить фокус в его инпут
		$(document).on('focusin', function(e) {
			if ($(e.target).closest(".mce-window").length) {
				e.stopImmediatePropagation();
			}
		});

		if ($('.autoclick').hasClass('ajax-get')) {
			appMain.ajaxLoadUrlHistory($('.autoclick').data('href'));
		} else if ($('.autoclick').hasClass('ajax-get-modal')) {
			appMain.ajaxLoadUrlModal($('.autoclick').data('href'));
		} else if ($('.autoclick').hasClass('ajax-post')) {
			$('.autoclick').click();
		}

		// ratchet socket connect
		if ((typeof socket == 'object') && (typeof self.appOptions.ratchet != 'undefined')) {
			socket.hashUser = self.appOptions.hashUser;
			socket.init(self.appOptions.ratchet);
		}

		if (self.appOptions.callbacks) {
			self.appOptions.callbacks.forEach(function(item, i, arr) {
				$.globalEval(item);
			});
		}

		if (self.appOptions.tutorialOptions) {
			self.showTutorial(self.appOptions.tutorialOptions);
		}
	}

	this.markMenuActiveByName = function(name)
	{
		$('.js-appmain-menu-depend-marker').each(function() {
			self.markMenuActive($(this).find('li a[data-menu-name="'+name+'"]'));
		});
	}

	/**
	 * Отдельная глобальная инициализация которая выполняется если страница перегружается по F5
	 * а также она же выполняется в ajaxSuccess на случай загрузки страницы через аякс
	 */
	this.initGlobalPlugins = function()
	{
		if (jQuery().areYouSure) {
			// dirty forms init
			$("form[data-init='dirty-check']").each(function() {
					var options = {};
					if ($(this).data('field-selector')) {
						options['fieldSelector'] = $(this).data('field-selector');
					}
					$(this).areYouSure(options);
				}
			);
		}

		if (jQuery().switcher) {
			$('.js-switcher-radio').switcher();
		}

		// определение page visibility
		$(document).on({
			'show-page': function() {
				self.pageVisible = 1;
			},
			'hide-page': function() {
				self.pageVisible = 0;
			}
		});


		/*DatePicker*/
		self.initDatePicker('input[data-init=datepicker]', false, false);

		/*Chosen*/
		self.initChosen("select[data-init='chosen-list']", false, false);

		/*scrollpane*/
//		self.initScrollPane();

		/* sound manager */
		self.initSoundManager();

		/* popover hint */
		self.initPopoverDefault();

		/* global rating bottom bar */
		typeof appWidgetRatingBarLocal!=='undefined' && appWidgetRatingBarLocal.updateGlobal();

		self.initVersionSwitcher();
		if (typeof window.Modernizr !== 'undefined') {
			self.initCalcEmulator();
			$(window).resize(function () {
				self.initCalcEmulator();
			});
		}
		// cache
		self.preloadAjaxGet();
	}

	this.initSoundManager = function()
	{
		if (typeof soundManager == 'undefined') return;
		soundManager.setup({
			url : self.appOptions.soundManagerConfig.baseUrl,
			debugMode: false,
			onready : function() {
				var pmSound = soundManager.createSound({
					id : 'pmSound',
					url : self.appOptions.soundManagerConfig.baseUrl+'/'+self.appOptions.soundManagerConfig.new_msg_file
				});
				var proposalSound = soundManager.createSound({
					id : 'proposalSound',
					url : self.appOptions.soundManagerConfig.baseUrl+'/'+self.appOptions.soundManagerConfig.new_proposal_file
				});
				var adminChatSound = soundManager.createSound({
					id : 'adminChatSound',
					url : self.appOptions.soundManagerConfig.baseUrl+'/'+self.appOptions.soundManagerConfig.admin_chat_file
				});
				//
				//mySound.play();
			},
			ontimeout : function() {
				// Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
			}
		});
	}

	this.initScrollPane = function()
	{
		$('.scroll-pane').jScrollPane();
	}

	this.initDatePicker = function(sel, func, cb){
		/*init DatePicker*/
		if (jQuery().datetimepicker) {
			$(sel).each(function(){
				var th = this;
				if($(th).is('.type-text')){
					var parentClass = 'type-text-wrap-input';
					var wrap = $('<div>',{
						'class':parentClass
					});

					$(th).attr("readonly","true");

					var iconClear = $("<i>",{
						'class': 'remove glyphicon glyphicon-remove',
						css: {
							left:$(th).width()+'px',
							'line-height':$(th).height()+10+'px'
						}
					}).on({
						click: function(){
							$(th).val('').trigger('changeDate');
						}
					});

					var label = $("<span>",{
						'class': 'inputLabel',
						html: $(th).data('label')?$(th).data('label'):''
					}).on('click', function(){
						$(th).datetimepicker('show');
					});

					$(th).wrap(wrap);

					var parent = $(th).parents('.'+parentClass);

					if($(th).val()!= ''){
						parent.addClass('has_date');
					}

					$(th).on({
						'show' : function(){
							$(this).addClass('active');
						},
						'hide' : function(){
							$(this).removeClass('active');
						},
						'changeDate' : function(){
							if(th.value == ''){
								parent.removeClass('has_date');
							}else{
								parent.addClass('has_date');
							}
							if(typeof cb == "function")
								cb(this);
						}
					});

					if (!$(th).data('hide-clear')) {
						parent.append(iconClear);
					}
					parent.append(label);
				}
				if(typeof func == "function")
					func(this);
				else{
					$(this).datetimepicker({
						autoclose: true,
						format: "d M yyyy",
						minView: 2,
						todayBtn: true
					});
				}
			});
		}
	}

	this.initChosen = function(selector, func, change){
		if (jQuery().chosen) {
			// beauty selector init
			$(selector).each(function(){
				var th = this;
				/*Получаем ширину*/
				var width = ($(this).data('width') ? $(this).data('width') : ($(this).width() > 0)? $(this).width()+16+'px' : '100%');

				if (typeof func == "function")
					func(this);
				else {
					var options = {disable_search_threshold: 500, width: width},
						placeholderTextSingle = $(this).data('placeholder_text_single');
					if (placeholderTextSingle) {
						options['placeholder_text_single'] = placeholderTextSingle;
					}
					$(this).chosen(options);
				}

				$(this).on({
					change: function(th){
						if(typeof change == "function")
							change(th);
					}
				});

				if($(th).is('.type-text')){
					var chos = $(th).siblings('.chosen-container');
					var time = false;

					chos.on('mouseleave', function(){
						if (self.timeoutId !== false) {
							clearTimeout(self.timeoutId);
						}
						time = setTimeout(function(){
							$(th).trigger('chosen:close');
						}, 500);
					});
				}
			});
		}
	}

	this.initVersionSwitcher = function () {
		$(document).on('click','.version-switcher a', function () {
			var href = $(this).attr('data-href');
			href += (href.indexOf('?') == -1 ? '?' : '&') + 'from=' + encodeURIComponent(document.location.pathname + document.location.search);
			window.location.replace(href);
			return false;
		});
	}

	//расчет относительных размеров на случай если браузер на поддерживает CSS calc()
	this.initCalcEmulator = function () {
		if (typeof Modernizr == 'undefined') return true;
		var always = Modernizr.csscalc ? "[data-calc-always]" : '';
		$('[data-calc-attribute]' + always).each(function () {
			var $this = $(this);
			var val = $this.data('calc-value') ? $this.data('calc-value') : eval($this.data('calc-expression'));
			$this.css($this.data('calc-attribute'), val)
		});
	}

	/**
	 * Обертка setter для сохранения данных виджета
	 * @param widgetName
	 * @param obj
	 */
	this.registerWidget = function(widgetName, obj)
	{
		self.regesteredWidgets[widgetName] = obj
	}

	this.ajaxLoadWidgetUrl = function (widgetName, actionName, params, callback)
	{
		if (typeof self.regesteredWidgets[widgetName] != 'undefined') {
			var url = self.regesteredWidgets[widgetName].url+'.'+actionName+'?'+$.param(self.regesteredWidgets[widgetName].params);
			if (typeof params == 'object') {
				url = url +'&'+$.param(params);
			}
			self.ajaxLoadUrl(url, undefined, undefined, callback);
		}
	}

	this.ajaxLoadUrlHistory = function(url, scrollto, e)
	{
		var execute=function() {
			url = decodeURIComponent(url);
			if (url != self.appOptions.currentUrl) {

				if (typeof e != 'undefined' && $(e.target).is('.autoclose-modal')) {
					self.closeModal();
				} else if (typeof e != 'undefined' && $(e.target).is('.autoclose-popup')) {
					$.alerts._hide();
				}

				if (window.History.enabled) {
					var obj = typeof scrollto == 'undefined' ? null : {scrollto:scrollto};
					History.pushState(obj, document.title, url);
				} else {
					self.ajaxLoadUrl(url, scrollto, {
						headers: {
							'Is-Modal-Request': 0,
							'Current-Layout': self.appOptions.currentLayout,
							'Resources-Version': self.appOptions.resourcesVersion,
							'Is-Match-Masks-List': JSON.stringify(self.appOptions.isMatchMasksList)
						}
					});
					self.appOptions.currentUrl = url;
				}
			} else {
				self.ajaxLoadUrl(url, scrollto, {
					headers: {
						'Is-Modal-Request': 0,
						'Current-Layout': self.appOptions.currentLayout,
						'Resources-Version': self.appOptions.resourcesVersion,
						'Is-Match-Masks-List': JSON.stringify(self.appOptions.isMatchMasksList)
					}
				});
			}
		}
		if (self.isFormDirty()) {
			jConfirm('Your data has not been saved. Are you sure you want to leave this page?', 'Please confirm this action', function(cResult) {
				if (cResult) {
					$('form').removeClass('dirty');
					execute();
				}
			});
		} else {
			execute();
		}
	}

	this.ajaxLoadUrlModal = function(url, scrollto, e)
	{
		self.ajaxLoadUrl(url, scrollto);
	}

	this.ajaxLoadUrl = function (url, scrollto, params, callback)
	{
		var ajaxParams = {
			"url": url,
			"type": "GET",
			"beforeSend": self.ajaxBeforeSendGet,

			"localCache": true,
			"cacheTTL": 1, // Optional. In hours.
			"isCacheValid": function() {
				return self.isUrlInCacheSchema(url);
			},

			"success": function(data) {
				//alert('success');
				self.ajaxSuccess('', data, url, scrollto);
				if (typeof callback == 'function') {
					callback();
				}
			},
			"error": self.ajaxError,
			"cache": false
		}

		var headers = {
			'Is-Modal-Request': 1,
			'Original-Route': self.appOptions.originalRoute,
			'Id-User': self.appOptions.idUser
		};
		if (typeof params != 'undefined') {
			if (typeof params.headers != 'undefined') {
				appUtils.extend(headers, params.headers);
			}
		}

		ajaxParams['headers'] = headers;

		$.ajax(ajaxParams);
	}

	this.isUrlInCacheSchema = function(url)
	{
		var isFind = false;
		$('.cache').each(function() {
			if (self.extraUriDecode(self.getUrl(this)) == self.extraUriDecode(url))
				isFind = true;
		});
		return isFind;
	}

	this.extraUriDecode = function(url)
	{
		return decodeURIComponent(decodeURIComponent(decodeURIComponent(url)));
	}

	this.preloadAjaxGet = function()
	{
		var urls = {}
		$('.cache').each(function() {
			var url = self.extraUriDecode(self.getUrl(this));
			urls[url] = 1;
		});

		localStorage.clear();

		$.each(urls, function(url) {
			$.ajax({
				"url": url,
				"type": "GET",

				"localCache": true,
				"cacheTTL": 1, // Optional. In hours.
				"isCacheValid": function() {
					return false;
				},
				"error": self.ajaxError,
				"cache": false,
				"headers": {
					'Is-Modal-Request': 1,
					'Original-Route': self.appOptions.originalRoute,
					'Id-User': self.appOptions.idUser
				}
			});
		});
	}

	this.isWidgetUrl = function(url)
	{
		if (url) {
			var spl = url.split('?'),
				params = $.deparam.querystring('?'+ (spl[1] || ''));

			if (params.widgetmarker)
				return true;
			else
				return false;
		} else {
			return false;
		}

	}

	this.btnAjaxBeforeSend = function(element)
	{
		if (typeof element.tagName != 'undefined' && element.tagName.toLowerCase() == 'button') {
			var btn = $(element);
			if(!btn.hasClass('do-not-block')){
				btn.attr('disabled', true);
			}
			// save original button width
			self.btnWidth = btn.width();
			// save original button label
			self.btnLabel = btn.html();
			// show progress indicator
			if (btn.hasClass('btn-animated')) {
				setTimeout(function() {
					btn.html('<i class="icon-upload"></i>');
				}, 500);
			} else {
				btn.html('<i class="icon-upload"></i>');
			}
			// restore original button width
			btn.width(self.btnWidth);
		}
		if (typeof element.tagName != 'undefined' && !$(element).hasClass('do-not-block')) {
			self.lockButton($(element));
		}
		$('.lock-full').addClass('in_progress_request');

		if ($(element).data('progress-icon')) {
			$(element).html('<i class="'+$(element).data('progress-icon')+'"></i>')
		}
	};

	this.lockButton = function($el)
	{
		$el.each(function() {
			$(this).addClass('in_progress_post');
		});
	}

	this.btnAjaxSuccess = function(element)
	{
		if (typeof element == 'string') {
			element = $(element)[0];
		}
		if (element && element.tagName.toLowerCase() == 'button') {
			var btn = $(element);
			//console.log(btn.attr('id'));
			// undisable button
			btn.attr('disabled', false);
			// save original button width
			self.btnWidth = btn.width();
			// restore saved button label
			btn.html(self.btnLabel);
			// restore original button width
			btn.width(self.btnWidth);
		}
		self.unlockButton($(element));
		$('.lock-full').removeClass('in_progress_request');
	}

	this.unlockButton = function($el)
	{
		$el.each(function() {
			$(this).removeClass('in_progress_post');
		});
	}

	this.ajaxError = function(jqXHR, textStatus, errorThrown)
	{
        var text = jqXHR.responseText;
        if (text) {
            if (typeof text == 'string' && text.toLowerCase().indexOf('sucuri') != -1) {
                jAlert('Access denied by Firewall');
                if (self.isButtonContext(this)) {
                    self.btnAjaxSuccess(this);
                }
                return;
            } else {
                alert(jqXHR.responseText);
            }
        }
		// enable button
		if (self.isButtonContext(this)) {
			self.btnAjaxSuccess(this);
		}
	}

	this.getUrl = function(element)
	{
		var url;
		if ($(element).attr('href')) {
			url = $(element).attr('href');
		} else if ($(element).attr('data-href')) {
			url = $(element).attr('data-href');
		} else {
			url = self.appOptions.currentUrl;
		}

		var widget;
		if (widget = $(element).data('widget')) {
			if (typeof self.regesteredWidgets[widget].params != 'undefined') {
				url += '?'+$.param(self.regesteredWidgets[widget].params);
			}
		}

		return url;
	}

	this.ajaxPost = function(element)
	{
		// confirm
		if ($(element).attr('data-confirm')) {
			if (!$(element).data('confirmed')) {
				jConfirm($(element).attr('data-confirm'), 'Please confirm this action', function(cResult) {
					if (cResult) {
						$(element).data('confirmed', true);
						self.ajaxPost(element);
					} else {
						if ($(element).attr('type') == 'checkbox') {
							// on fail: invert checkbox back
							if ($(element).is(':checked')) {
								$(element).prop('checked', false);
							} else {
								$(element).prop('checked', true);
							}
						}
					}
				});
			}
			if ($(element).data('confirmed')) {
				$(element).data('confirmed', false);
			} else {
				return false;
			}
		}

		// data and method
		var parent_form=$(element).parents("form");
		if (parent_form.length == 0 || $(element).is('.no_get_form')) {
			var data = $(element).clone().data(), method = false;
			data[self.appOptions.csrfTokenName] = self.appOptions.csrfTokenValue;
			if (data['confirm'] != 'undefined') delete data['confirm'];
			if (data['confirmed'] != 'undefined') delete data['confirmed'];
			if (data['aysOrig'] != 'undefined') delete data['aysOrig'];
			if (data['chosen'] != 'undefined') delete data['chosen'];
			if (data['width'] != 'undefined') delete data['width'];
			if (data['init'] != 'undefined') delete data['init'];
		} else {
			var data, method = parent_form.attr('method');

			// temporary remove form's token
			parent_form.find('input[name='+self.appOptions.csrfTokenName+']').remove();
			if (parent_form.find('input[name='+self.appOptions.csrfTokenName+']').length == 0) {
				parent_form.prepend($('<input>', {
					'type': 'hidden',
					'name': self.appOptions.csrfTokenName,
					'value': self.appOptions.csrfTokenValue
				}));
			}

			if (method == 'get')  {
				parent_form.find('input[name='+self.appOptions.csrfTokenName+']').remove();
			}

			// update form textarea from tinymce
			if (typeof tinyMCE != 'undefined') {
				tinyMCE.triggerSave();
			}

			data = parent_form.find(':input:not(.do-not-send)').serialize();
		}

		var url = self.getUrl(element);

		$.ajax({
			"url": url,
			"type": method ? method : "post",
			"context": element,
			"beforeSend": self.ajaxBeforeSendPost,
			"success": function(data) {
				self.ajaxSuccess(element, data, url);
			},
			"error": self.ajaxError,
			"data": data,
			"cache": false,
			"headers": {
				'Is-Modal-Request': 1,
				'Original-Route': self.appOptions.originalRoute
			}
		});

		return false;
	}

	this.ajaxSuccess = function (element, data, url, scrollto)
	{
		// обновим оригинальный route
		if (typeof data.originalRoute!=='undefined')
			self.appOptions.originalRoute = data.originalRoute;

		// обновим текущий layout
		if (typeof data.currentLayout!=='undefined' && !data.soft_redirect)
			self.appOptions.currentLayout = data.currentLayout;

		// save form id
		if (self.getFormSelector(element) !== false) {
			var formId = self.getFormSelector(element).attr('id');
		}

		// page title
		if (typeof data.metaTitle != 'undefined') {
			$('title').html(data.metaTitle);
		}

		// help content
		if (typeof data.helpContent != 'undefined') {
			$(data.helpContent.container).html(data.helpContent.content);
		}

		// container
		if (data.isLayoutContainerRestoring && self.layoutContainerSaved.isSaved) {

			$(self.layoutContainerSaved.selector).remove();
			var restoreContainer = self.layoutContainerSaved.selector + '__savedkey__',
				$restoreContainer = $(restoreContainer);

			var renameAttrs = function () {
				var sl = this;
				var newVal;
				$($(this)[0].attributes).each(function () {
					newVal = this.nodeValue;
					newVal = newVal.replace("__spacekey__", " ");
					newVal = newVal.replace("__savedkey__", "");
					$(sl).attr(this.nodeName, newVal);
				});
			}

			$restoreContainer.each(renameAttrs);
			$restoreContainer.find('*').each(renameAttrs);
			$restoreContainer.show();
			$('html,body').animate({scrollTop: self.layoutContainerSaved.scrollTop}, 0);
			$(window).off('scroll.wloadmore');
			$(window).on('scroll.wloadmore', self.layoutContainerSaved.windowScrollHandler);
			this.layoutContainerSaved = {
				isSaved: false,
				selector: false,
				scrollTop: 0,
				windowScrollHandler: null
			};
//console.log('Restored');

		} else if ((typeof data.container != 'undefined')&&(typeof data.content != 'undefined')) {

			if (data.isLayoutContainerSaving && !self.layoutContainerSaved.isSaved) {
				self.layoutContainerSaved.isSaved = true;
				self.layoutContainerSaved.selector = data.container;
				self.layoutContainerSaved.scrollTop = $(document).scrollTop();
				var windowScrollHandler;
				if (windowScrollHandler=appUtils.getEventHandler('scroll', 'wloadmore')) {
					self.layoutContainerSaved.windowScrollHandler = windowScrollHandler.handler;
				}

				var curContainer = self.layoutContainerSaved.selector,
					$curContainer = $(curContainer),
					newContainerAttrs = {};

				// убить если уже существует
				$(curContainer+'__savedkey__').remove();

				// собрать все атрибуты для нового контейнера
				$($curContainer[0].attributes).each(function() {
					newContainerAttrs[this.nodeName] = this.nodeValue;
				});

				var $newContainer = $('<div/>', newContainerAttrs);

				var renameAttrs = function() {
					var sl = this;
					var newVal;
					$($(this)[0].attributes).each(function() {
						if (this.nodeName != 'src' && this.nodeName != 'type') {
							newVal = this.nodeValue+'__savedkey__';
							newVal = newVal.replace(" ", "__spacekey__");
							$(sl).attr(this.nodeName, newVal);
						}
					});
				}

				$curContainer.each(renameAttrs);
				$curContainer.find('*').each(renameAttrs);
				$curContainer.hide();

				$newContainer.insertBefore($curContainer[0]);
//console.log('Saved');
			}

			// убиваем tinymce если такой имеется, чтобы потом повторная инициализация происходила корректно.
			// делаем это лишь в том случае, если не передана команда сохранить tinyMCE.
			if ((typeof tinyMCE != 'undefined') && (typeof data.saveMce == 'undefined')) {
				tinyMCE.remove();
			}

			if ($(data.container).data('pagecontainer')) {
				$(window)
					.off('scroll.wloadmore')
					.off('resize.pm')
				$(document).off('click');
				$('body').off('click.local');
				$('.help-hint-default').off('click').off('mouseenter').off('mouseleave');
				$('.help-hint-default, .js-hint, .js-popover-simple').popover('hide');
				$('.jvectormap-label').hide();
			}
			$(data.container).html(data.content);
			if (typeof scrollto != 'undefined') {
                var tuning;
                if (scrollto.indexOf('+') != -1) {
                    var scrollData = scrollto.split('+');
                    scrollto = scrollData[0];
                    tuning = parseFloat(scrollData[1]);
                }
                appUtils.scrollPage(scrollto, tuning);
			}
			self.initGlobalPlugins();
		}

		// callbacks
		if (typeof data.callback != 'undefined') {
			$.globalEval(data.callback);
		}

		// errors

		if (data.error) {
			self.isFormHasError = true;

			if (typeof data.errorInline != 'undefined' && data.errorInline) {
				var container = data.errorContainer ? data.errorContainer : '#error_inline_container';
				$(container).html(data.error);
				$(container).show();
			} else {
				var title;
				if (typeof data.errorTitle != 'undefined') {
					title = data.errorTitle;
				} else {
					title = 'Error';
				}
				jAlert(data.error, title);
			}

			// button enable
			if (self.isButtonContext(element)) {
				self.btnAjaxSuccess(element);
			}
		}

		// redirects
		if ((typeof data.soft_redirect!='undefined')) {
			if (formId) {
				$('#'+formId).removeClass('dirty');
			}
			if (!data.soft_redirect)
				data.soft_redirect = self.getUrl();

			if (typeof data.soft_redirect == 'object') {

				if ((typeof data.soft_redirect.scrollto!='undefined')&&(data.soft_redirect.scrollto)) {
					var scrollto = data.soft_redirect.scrollto;
				} else {
					var scrollto = '';
				}
				var targetUrl = data.soft_redirect.url ? data.soft_redirect.url : self.getUrl();
				if ((typeof data.soft_redirect.skip_history != 'undefined') && (data.soft_redirect.skip_history))
					self.ajaxLoadUrl(targetUrl, scrollto);
				else
					self.ajaxLoadUrlHistory(targetUrl, scrollto);

				if (data.soft_redirect.dependMenuItem) {
					self.markMenuActiveByName(data.soft_redirect.dependMenuItem);
				}

			} else {
				self.ajaxLoadUrlHistory(data.soft_redirect);
			}
		} else if ((typeof data.redirect!='undefined')&&(data.redirect)) {
			if (formId) {
				$('#'+formId).removeClass('dirty');
			}
			window.location = data.redirect;
		}

		// manage isDirty flag
		if (formId) {
			//if (self.isFormHasError) {
			//	if (self.isFormDirtyBeforeRequest) {
			//		$('#'+formId).addClass('dirty');
			//	} else {
			//		$('#'+formId).removeClass('dirty');
			//	}
			//} else {
			//	$('#'+formId).removeClass('dirty');
			//}
			//self.isFormDirtyBeforeRequest = null;
			self.isFormHasError = null;
		}

		// refresh captcha
		if (typeof data.hash1!='undefined' && typeof data.hash2!='undefined') {
			$('#captcha_general').attr('src', data['url']);
			$('body').data('captcha.hash', [data['hash1'], data['hash2']]);
		}

		$('.ajax-loader-progress').hide();

		if ($(element).hasClass('auto-enable')) {
			$(element).trigger('ajaxSuccessEvent');
			self.btnAjaxSuccess(element);
		}
	}

	this.ajaxBeforeSendCommon = function()
	{
		$('body > .popover').remove();
		$('.ajax-loader-progress').show();
	}

	this.ajaxBeforeSendGet = function()
	{
		self.ajaxBeforeSendCommon();
	}

	this.ajaxBeforeSendPost = function()
	{
		self.ajaxBeforeSendCommon();
		if (self.isButtonContext(this)) {
			self.btnAjaxBeforeSend(this);
			//self.isFormDirtyBeforeRequest = self.isFormDirty();
			//self.isFormHasError = false;
		}
	}

	this.isButtonContext = function(element)
	{
		if (element == '') {
			return false;
		} else if (typeof element.tagName == 'undefined') {
			return false;
		} else {
			return true;
		}
	}

	this.getFormSelector = function(element)
	{
		if (self.isButtonContext(element)) {
			if ($(element).parents("form").length > 0) {
				return $(element).parents("form");
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	this.setIsMathcMasksList = function(obj)
	{
		this.appOptions.isMatchMasksList = obj;
	}

	this.showToast = function(text, config)
	{
		var options;
		if (typeof config == 'object') {
			options = appUtils.extend(self.appOptions.toastMsgConfig, config);
		} else if (typeof config == 'string') {
			options = self.appOptions.toastMsgConfig;
			options.type = config;
		}
		options.text = text;
		$().toastmessage("showToast", options);
	}

	this.showScore = function(message, value)
	{
		self.scoreMessages.push({message:message, value:value});
		if (!self.scoreMessagesInProgress) self.displayScore();
	}

	this.displayScore = function()
	{
		if (!self.scoreMessages.length) {
			self.scoreMessagesInProgress = 0;
			return;
		}

		self.scoreMessagesInProgress = 1;
		var data = self.scoreMessages.shift();

		if (!$('#score_float_container').length) return;
		var container = $('#score_float_container');
		container.empty();
		container.append($('<div class="col-1"></div>').text(data.message));
		container.append($('<div class="col-2"></div>').text(data.value));
		container.append('<div class="clearfix"></div>');
		container.append('<div class="note">See your total score on "My Page"</div>');
		container.fadeIn(400).delay(2000).fadeOut(400, function() {
			self.displayScore();
		});
	}

	this.isFormDirty = function()
	{
		return $("form[data-init='dirty-check']").hasClass('dirty') /*|| (typeof tinyMCE != 'undefined' && tinyMCE.activeEditor != null && tinyMCE.activeEditor.isDirty())*/;
	}

	this.initModal = function(selector)
	{
		if (typeof selector == 'undefined') {
			selector = self.defaultModalLayerContainer;
		}

		$(selector).modal({show: false})
			.on('hide.bs.modal', function(e) {
				self.dirtyCheckEvent(e);
			})
			.on('shown.bs.modal', function() {
				// Исправление баги tinymce+bootstrap modal: сменить фокус с tinymce на обычный инпут формы
				// возможно только со второго клика. Первый клик переводит фокус на слой окна, а второй клик уже на input
				$(document).off('focusin.modal');

				// автофокус
				var focus = $(this).find("[autofocus]:first");
				if (focus.val()) {
					focus.select();
				} else {
					focus.focus();
				}

				/*Запрещаем закрытие окна, по клику вне окна*/
				$(selector).off('click.dismiss.bs.modal');

				/*Назначаем закрітие по mousedown вне окна*/
				$(selector).on('mousedown.dismiss.bs.modal', function (e) {
					if ($(e.target).is('.modal') || $(e.target).is('.title-close,.button-close')) {
						if ($(e.target).find('div[data-backdrop=static]').length==0)
							$(selector).modal('hide');
					}
				});
			});
	}

	this.isModalHidden = function(selector)
	{
		if (typeof selector == 'undefined') {
			selector = self.defaultModalLayerContainer;
		}
		return $(selector).is(':hidden');;
	}

	this.openModal = function(selector)
	{
		if (typeof selector == 'undefined') {
			selector = self.defaultModalLayerContainer;
		}

		// установка ширины окна опираясь на ширину контента
		var width = $(selector).find('.modal-content > *:not(.modal-title):first').width();
		if (width > 0) {
			$(selector).find('.modal-dialog').width(width);
		}

		// открыть модальное окно
		$(selector).find('.title-close').show();
		if ($(selector).find('div[data-hide-close=1]').length) {
			$(selector).find('.title-close').hide();
		}
		//$(selector).modal(typeof options=='undefined' ? 'show' : options);
		var options = {};
		var backdrop = $(selector).find('div[data-backdrop]');
		if (backdrop.length) options.backdrop = backdrop.data('backdrop');
		var keyboard = $(selector).find('div[data-keyboard]');
		if (keyboard.length) options.keyboard = keyboard.data('keyboard');

		if (!$.isEmptyObject(options)) {
			$(selector).removeData("bs.modal").modal(options);
		}
		else {
			$(selector).modal('show');
		}
	}

	this.hideModalCloseLink = function()
	{
		$('#modal_dialog_general .title-close').hide();
	}

	this.closeModal = function(selector)
	{
		if (typeof selector == 'undefined') {
			selector = self.defaultModalLayerContainer;
		}

		// remove dirty
		var form = $(selector).find("form[data-init='dirty-check']");
		$(form).removeClass('dirty');

		$(selector).modal('hide');
		self.afterCloseModal(selector);
	}

	this.afterCloseModal = function(selector, event)
	{
		$(selector).find('.modal-content').empty();
	}

	this.dirtyCheckEvent = function (event) {
		var form = $(event.target).find("form[data-init='dirty-check']");
		var that = event.target;
		if (form.length > 0 && form.hasClass('dirty')) {
			jConfirm('Your data has not been saved. Are you sure you want to close this dialog?', 'Confirm dialog', function(cResult) {
				if (cResult) {
					$(form).removeClass('dirty');
					$(that).modal('hide');
				}
			});
			event.preventDefault();
		} else {
			self.afterCloseModal(that, event);
		}
	}

	this.initTinyMCE = function(config, configName)
	{
		if (typeof configName == 'undefined') {
			configName = 'default';
		}

		if (typeof config != 'undefined') {
			var conf = $.extend(false, {}, this.tinyMceConfig[configName], config);
		}

		if (typeof tinyMCE != 'undefined') {
			tinyMCE.remove();
		}
		tinyMCE.init(conf);
	};

	this.initRedactor = function (config, configName) {
		if (typeof configName == 'undefined') {
			configName = 'default';
		}
		var conf = this.redactorConfig[configName];
		if (typeof config != 'undefined') {
			conf = $.extend(false, {}, this.redactorConfig[configName], config);
		}
		var el = $('.redactor');
		if (conf.selector) {
			el = $(conf.selector);
		}
		if (typeof $.fn.redactor == 'function' && !el.attr('has-redactor')) {
			el.redactor(conf);
			el.attr('has-redactor', 1);
		}
	};

	this.initPopoverDefault = function()
	{
		// var 1
		$('.help-hint-default').popover({html: true});

		$('.help-hint-default').on('mouseenter', function (event) {
			$('.help-hint-default').not(this).popover('hide');
			event.stopPropagation();
			event.preventDefault();
		}).on('click', function (event) {
			$('.help-hint-default').not(this).popover('hide');
			if (!$(this).data('propagation')) {
				event.stopPropagation();
				event.preventDefault();
			}
		});

		$(document).on('mouseleave', function() {
			$('.help-hint-default').popover('hide');
		}).on('click', function () {
			$('.help-hint-default').popover('hide');
		});

		// var 2 - simple
		$('.js-popover-simple').popover({html: true});
	}
	this.raPlaySound = function(topic, data)
	{
		if (data.isSound>0) {
			soundManager.play(data.idSound);
		}
	}
	this.markMenuActive = function($el)
	{
		var $targetLi = $el.parents('li');
		if ($targetLi.length>0) {
			if ($targetLi.parents('.js-appmain-menu-depend-marker').length) {
				$('.js-appmain-menu-depend-marker li').removeClass('active-menu-item');
			}
			$targetLi.addClass('active-menu-item');
		}
	}

	this.hideTutorial = function()
	{
		if ($.tutorialize.getCurrentIndex() != -1) {
			$.tutorialize.stop();
		}
	}
	this.showTutorial = function(options)
	{
		self.hideTutorial();

		for (key in options.slides) {
			if (!$(options.slides[key].selector).length) {
				return;
			}
		}

		// default
		var _options = {
			showButtonClose: false,
			arrowPath: '/media/libs/tutorialize/arrows/arrow-gray2.png',
			overlayMode: 'focus',
			title: '',
			overlayOpacity: 0.1,
			bgColor: "#555",
			fontSize: '14px',
			borderRadius: '2px',
			arrowSize: 16

		}
		appUtils.extend(_options, options);
		$.tutorialize(_options);
		$.tutorialize.start();
	}

	this.raOnline = function(topic, data)
	{
		if (data.is_online == '1') {
			$('.js-user-'+data.id).addClass('hidden');
		} else if (data.is_online == '0') {
			$('.js-user-'+data.id).removeClass('hidden');
		}
	}

	this.raAll = function(topic, data)
	{
		if (data.reload) {
			window.location.reload();
		}
	}

	this.simplePost = function (url, data, callback)
	{
		data = data || {};
		data[self.appOptions.csrfTokenName] = self.appOptions.csrfTokenValue;
		$.post(url, data, callback);
	}

	this.replaceLngUrl = function(str)
	{
		// get 100% relative url
		url = document.createElement('a');
		url.href = str;
		url = url.pathname+url.search;

		var arr = url.split('/');
		arr.splice(0, 1); // remove first empty el

		if (arr[0] && arr[0].length == 2 && appUtils.in_array(arr[0], self.appOptions.languageKeys)) {
			arr.splice(0, 1);
		}

		url = '/'+arr.join('/');
		if (self.appOptions.defaultLanguage != self.appOptions.language) {
			url = '/'+self.appOptions.language+url;
		}
		return url;
	}
}
var appMain = new AppMain();