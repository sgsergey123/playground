<?php
return array(
	'components'=>array(
		'db'=>array(
			'class'=>'system.db.CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=playground_udimi',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
			'enableProfiling' => true,
			'enableParamLogging' => true,
		),
		'securityManager' => array(
			'encryptionKey' => 'w784tbfyqwiefwef',
			'validationKey' => 'jeh54wgeqrferfer',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
//				'yii-debug' => array(
//					'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
//					'ipFilters'=>array('::1','91.244.25.241'),
//				),
//				'db' => array(
//					'class' => 'CWebLogRoute',
//					'categories' => 'system.db.CDbCommand',
//					'showInFireBug' => false,
//				),
			),
		),
	),
	'params'=>array(
		'is_local' => true,
		'site_host' => 'http://playground.udimi.loc',
		'system_domain' => 'playground.udimi.loc',
		'ssl_only'=>0,
		'server_down'=>0,
	),
);