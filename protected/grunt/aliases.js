module.exports = {
	'default': [
		'prod'
	],
	'prod': [
		'dev',
		'uglify',
		'cssmin'
	],
	'dev': [
		'concatJs',
		'concatCss',
	],
	'concatJs': [
		'concat:memberarea_js',
		'concat:guestarea_js'
	],
	'concatCss': [
		'css_url_relative',
		'concat:memberarea_css',
		'concat:guestarea_css'
	],
	'fontello': [
		'fontelloUpdate',
		'dev'
	],
};