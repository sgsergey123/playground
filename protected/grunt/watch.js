module.exports = {

	options: {
		spawn: false,
		livereload: true
	},

	js: {
		files: [
			'assets/**/*.js',
			'modules/**/*.js',
			'components/widgets/**/*.js',
		],
		tasks: [
			'concatJs'
		]
	},

	css: {
		files: [
			'runtime/css-compiled/*.css'
		],
		tasks: [
			'concatCss'
		]
	},

	libs: {
		files: [
			'../httpdocs/media/libs/**/*.css',
			'../httpdocs/media/libs/**/*.js'
		],
		tasks: [
			'dev'
		]
	},
};