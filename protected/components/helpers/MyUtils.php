<?php
class MyUtils
{
	public function text2Snippet($str)
	{
		return str_replace(array("\n", "\t"), array(" ", " "), $str);
	}

	public static function numericLabel($value, $label, $htmlOptions=array())
	{
		return CHtml::tag('span', $htmlOptions, $value).' '.($value ==1 ? $label : $label.'s');
	}

	public static function getRemoteAddr()
	{
		if ($_SERVER['REMOTE_ADDR'] == '::1') {
			$ip = '127.0.0.1';
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	public static function getWordsCnt($str)
	{
		$str = str_replace(array("\r\n", "\n", "\r"), array(" ", " ", " "), $str);
		$str = str_replace(array(",", "."), array(" ", " "), $str);
		$str = trim($str);
		$public_comment_words = $str ? explode(" ", $str) : array();
		return sizeof($public_comment_words);
	}

	/**
	 * Вывод фденежного формата.
	 * @param $amount
	 * @param $params
	 *
	 * @return string
	 */
	public static function money_format($amount, $params = array())
	{
		if ($amount < 0) {
			$value = '-$'.number_format(abs($amount),2,'.',' ');
			$class = '';
		}
		elseif ($amount == 0) {
			$value = '$'.number_format($amount,2,'.',' ');
			$class = 'grey-color';
		}
		else {
			$value = '$'.number_format($amount,2,'.',' ');
			$class = 'green-color';
		}

		if (isset($params['additionalClass'])) {
			$class .= ' '.$params['additionalClass'];
		}

		if (isset($params['withoutSpan']) && $params['withoutSpan']) {
			$html = $value;
		} else {
			$html = '<span class="'.$class.'">'.$value.'</span>';
		}

		return $html;
	}

	public static function moneyFormat($amount, $params = array())
	{
		if ($amount < 0) {
			$value = '-$'.number_format(abs($amount),2,'.',' ');
			$class = '';
		}
		elseif ($amount == 0) {
			$value = '$'.number_format($amount,2,'.',' ');
			$class = 'grey-color';
		}
		else {
			$value = '$'.number_format($amount,2,'.',' ');
			$class = 'green-color';
		}

		if (isset($params['additionalClass'])) {
			$class .= ' '.$params['additionalClass'];
		}

		if (!empty($params['span'])) {
			$html = '<span class="'.$class.'">'.$value.'</span>';
		} else {
			$html = $value;
		}

		return $html;
	}
	/**
	 * Метод ищет в валидаторе модели потенциально установленую максимальную длину строки.
	 * @param $model
	 * @param $attribute
	 *
	 * @return int|string
	 */
	public static function getValidatorMaxLength($model, $attribute)
	{
		$maxlength = false;
		foreach($model->getValidators($attribute) as $validator)
		{
			if($validator instanceof CStringValidator && $validator->max!==null)
			{
				$maxlength=$validator->max;
				break;
			}
		}
		return $maxlength;
	}

	public static function getFirstError($model, $is_attribute=false)
	{
		$errors = $model->getErrors();
		if (!$errors) {
			if (method_exists($model, 'relations')) {
				$relations = $model->relations();
				foreach ($relations as $relationName => $relationParams)
				{
					if ($relationParams[0] == 'CHasManyRelation') {
						foreach ($model->$relationName as $relatedModel)
							$errors = CMap::mergeArray($errors, $relatedModel->getErrors());
					} else if ($model->$relationName) {
						$errors = CMap::mergeArray($errors, $model->$relationName->getErrors());
					}
					if ($errors) break;
				}
			}
		}

		if ($errors) {
			$attribute = key($errors);
			$firstAttributeErrors = array_shift($errors);
			$value = array_shift($firstAttributeErrors);
			return $is_attribute ? ['attribute'=>$attribute, 'error'=>$value] : $value;
		}
		else
			return false;
	}

	public static function getRatePercent($a, $b, $percision=2, $raw=false)
	{
		$result = 0;
		if ($a != 0) {
			$result = round($b/$a*100, 2);
		}
		return $raw ? $result : number_format($result, $percision).'%';
	}

	public static function get_redirects($url, $format = 1, $current_depth_level = 0, $max_depth_level = 10)
	{
		return HeadersParser::getLocations($url, $max_depth_level);
	}

	public static function generateUniqueStringUid($length, $tbl, $field='uid', $cond=1, $params=array()) {
		$exist = 1;
		while ($exist) {
			$string = self::generateRandString($length);
			$exist = Yii::app()->db->createCommand()
				->select('id')
				->from($tbl)
				->where('`'.$field.'`=:uid AND '.$cond, CMap::mergeArray(array(':uid'=>$string), $params))
				->queryRow();
		}
		return $string;
	}

	public static function generateRandString($length) {
		if (is_array($length)) {
			$len = $length['length'];
			$allowed_chars = $length['allowed_chars'];
		} else {
			$len = $length;
			$allowed_chars = 'abcdefghijklmnopqrstuvyxwz123456789';
		}
		$numChars = strlen($allowed_chars);
		$string = '';
		for ($i = 0; $i < $len; $i++) {
			$string .= substr($allowed_chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}

	/**
	 * Специфический открывающийся тег div для вьюхи вечного скролла с плавающей постраничкой.
	 * За этот тип постранички отвечает виджет wPager
	 * @param                     $i
	 * @param CActiveDataProvider $dataProvider
	 * @param string              $class
	 *
	 * @return string
	 */
	public static function openDivFloatPagination($i, CActiveDataProvider $dataProvider, $htmlOptions=array())
	{
		$options = array();

		if ($dataProvider->pagination !== false) {
			if (in_array($i, array(0, $dataProvider->pagination->pageSize - 1))) {
				$classAdditional = 'float-pagination-item';
				$options['data-page'] = $dataProvider->pagination->currentPage + 1;
			} else {
				$classAdditional = false;
			}
		} else {
			$classAdditional = false;
		}

		if (!empty($htmlOptions['class'])) {
			$options['class'] = $htmlOptions['class'].(!empty($classAdditional) ? ' '.$classAdditional : '');
		} else {
			$options['class'] = $classAdditional;
		}

		return CHtml::openTag('div', CMap::mergeArray($htmlOptions, $options));
	}

	/**
	 * Позволяет передавать атрибуты для option при построении dropDownList
	 *
	 * @param $name
	 * @param $selected
	 * @param $data
	 * @param $htmlOptions
	 *
	 * @return string
	 */
	public static function extendedDropDownList($name, $selected, $data, $htmlOptions)
	{
		$listData = $options = array();
		foreach ($data as $k => $val) {
			$listData[$k] = $val['label'];
			$options[$k] = $val['optionAttributes'];
		}
		$htmlOptions['options'] = $options;
		return CHtml::dropDownList($name, $selected, $listData, $htmlOptions);
	}

	/**
	 * Группирует массив объектов по одному их общему свойству
	 * @param $models
	 * @param $attr
	 *
	 * @return array
	 */
	public static function groupModels($models, $attr)
	{
		$ret = array();
		foreach ($models as $model) {
			if (!isset($ret[$model->$attr])) {
				$ret[$model->$attr] = array();
			}
			$ret[$model->$attr][] = $model;
		}
		return $ret;
	}

	/**
	 * делает то же, что CHtml::listData, но принимает callable в качестве параметра
	 *
	 * @param array $models
	 * @param string $name
	 * @param callable $value
	 *
	 * @return array
	 */
	public static function extendedListData($models, $name, $value)
	{
		$ret = array();
		foreach($models as $model){
			$ret[$model->$name] = $value($model);
		}
		return $ret;
	}

	/**
	 * Получить номер позиции (на каком месте юзер) в разрезе переданного поля
	 * @param $table
	 * @param $field
	 * @param $id_user
	 */
	public static function getNumberPosition($table, $fieldWithSort, $condFieldName, $condFieldValue, $cacheHours=1)
	{
		$command = Yii::app()->db->cache($cacheHours*60*60)->createCommand("
			SELECT `number` FROM (
				SELECT
					@num:=@num+1 as `number`,
					".$condFieldName."
				FROM `".$table."`, (SELECT @num := 0) as t
				ORDER BY ".$fieldWithSort."
			) as `temp_table`
			WHERE ".$condFieldName."=".$condFieldValue."
		");
		$number = $command->queryScalar();
		return $number>100 ? '100+' : $number;
	}

	public static function var_dump($var)
	{
		throw new CException(var_export($var, true));
	}

	public function encodeSafeUrl($url)
	{
		return urlencode(str_replace('/', '^|^', $url));
	}

	public function decodeSafeUrl($url)
	{
		return str_replace('^|^', '/', urldecode($url));
	}

	// setlocale(LC_ALL, 'en_US.UTF8');
	public static function toAscii($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = trim($clean, '-');
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	public static function BlogLink($alias, $linkText=false, $htmlOptions=[])
	{
		$blogUrl = 'https://blog.udimi.com/';

		$blogUrlsAliased = [
			'blog'=>'',
			'prime-membership' => 'prime-membership',
			'optin-tracking' => 'optin-tracking',
			'what-is-a-swipe' => 'what-is-a-swipe',
			'affiliate-program' => 'affiliate-program',
			'how-to-reply-by-email' => 'how-to-reply-by-email',
			'common-reasons-for-rejected-solo-orders' => 'common-reasons-for-rejected-solo-orders',
			'udimi-tos' => 'udimi-tos',
			'terms-and-conditions'=>'udimi-tos#terms-and-conditions',
			'privacy-policy'=>'udimi-tos#privacy-policy',
			'how-to-sell' => 'how-to-sell',
			'how-to-sell#extras' => 'how-to-sell/#extras',
			'affiliate-program-for-sellers' => 'affiliate-program-for-sellers',
			'custom-affiliate-agreements' => 'custom-affiliate-agreements',
			'price-difference' => 'price-difference',
			'reseller' => 'reseller',
			'reseller-own-domain-name' => 'reseller-own-domain-name',
			'delivery-terms-and-conditions' => 'delivery-terms-and-conditions',
			'how-to-buy-a-solo' => 'how-to-buy-a-solo',
			'reseller-landing-page-creator' => 'reseller-landing-page-creator',
			'reseller-api' => 'reseller-api',
			'reseller-faq' => 'reseller-faq',
			'reseller-automation' => 'reseller-automation',
		];

		$url = $blogUrl.(empty($blogUrlsAliased[$alias]) ? '' : $blogUrlsAliased[$alias]);

		if ($linkText) {
			$htmlOptions['target'] = '_blank';
			$htmlOptions['encode'] = true;
			return CHtml::link($linkText, $url, $htmlOptions);
		} else {
			return $url;
		}
	}

	public static function getBlogContent($alias, $params=[], $force=false)
	{
		require_once(Yii::getPathOfAlias('application.components').'/simple_html_dom.php');
		$cacheId = "udimi-blog-{$alias}";
		$content = $force ? false : Yii::app()->cache->get($cacheId);

		if ($content === false) {
			if (!$content = self::getRemoteContent(MyUtils::BlogLink($alias), 2)) return false;
//			$html = file_get_html($url);
			$html = str_get_html($content);
			$node = $html->find('div.entry-content', 0);

			if (!empty($params['maxImageWidth'])) {
				foreach($html->find('img[srcset]') as $img) {
					$srcset = $img->srcset;
					$srcsetArr = explode(', ', $srcset);
					$indexed = [];
					foreach ($srcsetArr as $item) {
						list($itemSrc, $w) = explode(' ', $item);
						$w = intval($w);
						$indexed[intval($w)] = $itemSrc;
					}
					krsort($indexed);

					$max = max(array_keys($indexed));

					$img->src = $indexed[$max];
					$k = $img->height/$img->width;
					
					$img->width = $params['maxImageWidth'];
					$img->height = round($params['maxImageWidth']*$k);

					$img->srcset = null;
					$img->sizes = null;
				}
			}

//			$images = [];
//			if ($node && $embedImages) {
//				$imgs = $node->find('img');
//				foreach ($imgs as $img) {
//					$img->removeAttribute('srcset');
//					$img->removeAttribute('sizes');
//					$images[basename($img->src)] = [
//						'string'=>file_get_contents($img->src),
//						'src'=>$img->src,
//					];
//				}
//			}

			$content = $node ? $node->innertext : '';
			$html->clear();
			unset($html);

			Yii::app()->cache->set($cacheId, $content, 24*3600);
		}
		return $content;
	}

	public static function getRemoteContent($url, $timeout=0)
	{
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if ($timeout) {
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_TIMEOUT , $timeout);
		}
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result=curl_exec($ch);
		curl_close($ch);
		return $result;
	}


	public static function removeOutliers($dataset, $magnitude = 1) {

		$count = count($dataset);
		$sum = 0;
		foreach ($dataset as $row) $sum += $row['t'];
//		$sum = array_sum(array_column($dataset, 't'));

		if (!$count || !$sum) return $dataset;
		$mean = $sum / $count; // Calculate the mean
		$deviation = sqrt(array_sum(array_map(['self', "sd_square"], $dataset, array_fill(0, $count, $mean))) / $count) * $magnitude; // Calculate standard deviation and times by magnitude

//		echo "mean $mean<br>";
//		echo "mean $deviation<br>";

		return array_values(array_filter($dataset, function($x) use ($mean, $deviation) {
			return ($x['t'] <= $mean + $deviation && $x['t'] >= $mean - $deviation);
		})); // Return filtered array of values that lie within $mean +- $deviation.
	}

	public static function sd_square($x, $mean)
	{
		return pow($x['t'] - $mean, 2);
	}

	public static function sign($data)
	{
		$private_key = 'AkbiZWCi14K7FHK30T7o';
		if (is_array($data)) {
			$data = implode('', $data);
		}
		return base64_encode( sha1( $private_key . $data . $private_key, 1 ) );
	}

	public static function likeEscape($keyword)
	{
		return '%'.strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%';
	}

	public static function o()
	{
		echo '<pre>';
		ob_start();
	}

	public static function c()
	{
		$content = ob_get_contents();
		ob_clean();
		echo htmlspecialchars($content, ENT_QUOTES, 'UTF-8');
		echo '</pre>';
		exit;
	}

	public static function appendSchema($str)
	{
		if (strpos($str, 'http://') === false && strpos($str, 'https://') === false && trim($str)) {
			return 'http://'.trim($str);
		} else {
			return $str;
		}
	}

	public static function removeEmptySchema($str)
	{
		if ((strpos($str, 'http://') !== false || strpos($str, 'https://') !== false)) {
			$res = str_replace(['http://', 'https://'], ['', ''], $str);
			return empty($res) ? '' : $str;
		} else {
			return $str;
		}
	}

	public static function removeZeroFractional($str)
	{
		$strNew = str_replace(',', '.', $str);
		$p = mb_stristr($strNew, '.', false);
		$p = str_replace('.', '', $p);
		$p = intval($p);
		if ($p) {
			return $str;
		} else {
			return mb_stristr($strNew, '.', true);
		}
	}

	/**
	 * Yii::configure() from Yii2 analog
	 */
	public static function configure($obj, $props)
	{
		foreach ($props as $propName => $propVal) {
			$obj->$propName = $propVal;
		}
		return $obj;
	}

	public static function getUserLng($weightNum=0)
	{
		if ($list = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']) : null) {
			if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
				$lng = array_combine($list[1], $list[2]);
				foreach ($lng as $n => $v) {
					$lng[$n] = $v ? $v : 1;
				}
				arsort($lng, SORT_NUMERIC);
				$lng = array_keys($lng);
				return isset($lng[$weightNum]) ? $lng[$weightNum] : '';
			}
		}

		return '';
	}

	public static function convert2db($str)
	{
		$res = htmlentities($str, ENT_NOQUOTES, 'UTF-8', false);
		return str_replace('&amp;', '&', $res);
	}

	public static function coma2dot($str)
	{
		return str_replace(',', '.', $str);
	}

	public static function createAttrArray($models, $attrName, $json=false, $callback=null)
	{
		$arr = [];
		foreach ($models as $model) {
			$value = $callback ? $callback($model->$attrName) : $model->$attrName;
			$arr[] = $value;
		}
		return $json ? json_encode($arr) : $arr;
	}

	public static function arrayToMask($array=[], $total=31)
	{
		if (!is_array($array)) $array = [$array];
		$mask = 0;
		foreach ($array as $item) {
			if ($item > $total)
				throw new CException('Wrong item index');
			$mask = $mask | 1<<($item-1);
		}
		return $mask;
	}

	public static function maskToArray($mask, $total=31)
	{
		$array = [];
		for ($i=0; $i < $total; $i++) {
			$x = 1 << $i;
			if ($x & $mask) $array[] = $i+1;
		}
		return $array;
	}

    public static function transliterate($textcyr = null, $textlat = null)
    {
        $cyr = [
            'ж', 'ч', 'щ', 'ш', 'ю', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я',
            'Ж', 'Ч', 'Щ', 'Ш', 'Ю', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я'];
        $lat = [
            'zh', 'ch', 'sht', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', 'x', 'q',
            'Zh', 'Ch', 'Sht', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q'];
        if ($textcyr) return str_replace($cyr, $lat, $textcyr);
        else if ($textlat) return str_replace($lat, $cyr, $textlat);
        else return null;
    }

	public static function getFirstName($name)
	{
 		return ucfirst(mb_substr($name, 0, mb_strpos($name,' ') ? mb_strpos($name,' ') : mb_strlen($name)));
	}

	public static function getYouTubePattern()
	{
		return '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
	}
}