<?php
class Controller extends CController
{
	public $metaKeywords = 'Buy solos, sell solos, buy email solo ad, email solo ads, Udimi';
	public $metaDescription = 'Buy Email Solo Ads or Sell Solos. FREE to buy. FREE to sell.';
	public $metaTitle = 'Buy Solo Ads | Udimi';

	/**
	 * @var array Ajax unload callbacks
	 * Ex: 'namedMask'=>['obj1.Method1()', 'obj2.Method2()'];
	 * 'namedMask' should be added to main.php config
	 */
	public $onAjaxUnload = [
	];

	public $baseUrlImg;
	public $baseUrlLibs;
	public $baseUrlVideos;

	public $pageTitle;
	public $skipTopMenu = false;
	public $skipFbOgTags = false;
	public $favicon;

	protected $jsonResponseData = array();

	/**
	 * @var array отправляется объектом в глобальный js приложения
	 */
	public $jsAppOptions;

	/**
	 * @var string контейнер в который аяксом загружается страница. Обрамляет переменную $content
	 */
	public $layoutContainer = '#layout_content';
	public $isLayoutContainerSaving = false;
	public $isLayoutContainerRestoring = false;

	/**
	 * @var string имя пакета ресурсов, собранного для главного лэйаута
	 */
	public $layoutPackageName;

	/**
	 * @var bool В этом режиме несмотря на то что юзер залогинен, top menu выводится как для незалогиненного
	 * а в левой колонке выводится только кнопка-ссылка Register
	 */
	public $layoutPreviewMode = 0;

	/**
	 * @var массив actions которые необходимо обернуть транзакцией. Определеятся в контроллере потомке.
	 */
	public $transactionExcludeActions;

	/**
	 * @var массив actions которые необходимо обернуть транзакцией. Определеятся в контроллере потомке.
	 */
	public $sslRedirectExcludeActions;

	public $softRedirectParams;

	/**
	 * @var Хеш-версия ресурсов (js и css). Контрольная сумма дат модификаций файлов.
	 */
	private $_resourcesVersion;
	private $_currentPageMask;

	public function init()
	{
		$this->baseUrlImg = Yii::app()->request->getBaseUrl().'/media/img';
		$this->baseUrlLibs = Yii::app()->request->getBaseUrl().'/media/libs';
		$this->baseUrlVideos = Yii::app()->request->getBaseUrl().'/media/videos';

		$this->_resourcesVersion = $this->getResourcesVersion();

		$this->favicon = $this->baseUrlImg."/favicon.png";

		$this->jsAppOptions = array(
			'resourcesVersion' => $this->_resourcesVersion,
			'csrfTokenName' => Yii::app()->request->csrfTokenName,
			'csrfTokenValue' => Yii::app()->request->csrfToken,
			'currentUrl' => Yii::app()->request->originalUrl,
			'toastMsgConfig' => array(
				'position' => 'bottom-right',
				'type' => 'notice',
				'sticky' => false,
				'close' => new CJavaScriptExpression('function () {}'),
				'stayTime' => 3000,
			),
			'soundManagerConfig'=>array(
				'baseUrl' => $this->baseUrlLibs.'/soundmanager',
				'new_msg_file' => 'newmsg_vk.mp3',
				'new_proposal_file' => 'moneta.mp3',
				'admin_chat_file' => 'admin_chat.mp3',
				'huy_file' => 'huy.mp3'
			),
			'idUser' => Yii::app()->user->isGuest ? null : Yii::app()->user->id,
			'hashUser' => Yii::app()->user->isGuest ? session_id() : ((!empty(Yii::app()->user)&&!empty(Yii::app()->user->model)&&!empty(Yii::app()->user->model->hash)) ? Yii::app()->user->model->hash : null),
			'translatedLanguages'=>Yii::app()->params['translatedLanguages'],
			'languageKeys'=>array_keys(Yii::app()->params['translatedLanguages']),
			'defaultLanguage'=>Yii::app()->params['defaultLanguage'],
			'language'=>Yii::app()->getLanguage(),
		);

		if (Yii::app()->user->isGuest) {
			$this->layout='//layouts/col1center';
			$this->layoutPackageName = 'guestarea';
		} else {
			$this->layout= '//layouts/col2';
			$this->layoutPackageName = 'memberarea';

			Yii::app()->setComponents(array(
				'userConfig'=>array(
					'class'=>'DUserConfig',
					'id_user'=>Yii::app()->user->id,
				),
			));
		}

		if (isset($_GET['lc_s'])) {
			$this->isLayoutContainerSaving = true;
		}
		if (isset($_GET['lc_r'])) {
			$this->isLayoutContainerRestoring = true;
		}

		if (isset($_GET['widgetmarker']) && $_GET['widgetmarker'] == 1) {
			return false;
		} else {
			return true;
		}
	}

	public function actions()
	{
		$result = array();

		$result['captcha'] = array(
			'class'=>'CaptchaAction',
			'foreColor'=>0x215884,
			'transparent'=>true,
			'width'=>106,
		);
		if (Yii::app()->user->isGuest) {
			// Только для незалогиненных
		} else {
			// Только для залогиненных
			$result['wmenuleft.'] = array(
				'class' => 'application.components.widgets.wMenuLeft.wMenuLeft',
			);
		}
		// И для залогиненных и для незалогиненных
		$result['wmenutop.'] = array(
			'class' => 'application.components.widgets.wMenuTop.wMenuTop',
		);
		$result['wmenulogo.'] = array(
			'class' => 'application.components.widgets.wMenuLogo.wMenuLogo',
		);
		return $result;
	}

	/**
	 * По условию переопределяем метод ядра Yii с целью заворачивания всего action в транзакцию
	 * @param string $actionID
	 *
	 * @throws Exception
	 */
	public function run($actionID)
	{
		if (!empty($this->getModule()->id) && $this->getModule()->ignoreHttpsRedirect) {
			$ignoreHttpsRedirect = true;
		} else {
			$ignoreHttpsRedirect = false;
		}

		if (
			($this->sslRedirectExcludeActions === null || !is_array($this->sslRedirectExcludeActions) || !in_array($actionID, $this->sslRedirectExcludeActions)) && !$ignoreHttpsRedirect
		) {
			if ((!empty(Yii::app()->params['ssl_only']))&&(!Yii::app()->request->isSecureConnection)) {
				$url = Yii::app()->request->getHostInfo('https').Yii::app()->request->originalUrl;
				Yii::app()->request->redirect($url, true);
			}
		}

		//начинается всё с копипасты из ядра yii:
		if(($action=$this->createAction($actionID))!==null) {
			if(($parent=$this->getModule())===null)
				$parent=Yii::app();
			if($parent->beforeControllerAction($this,$action)) {
				if ($this->transactionExcludeActions !== null && is_array($this->transactionExcludeActions) && in_array($actionID, $this->transactionExcludeActions)) {
					$this->jsAppOptions['originalRoute'] = $this->getRoute($action);
					$this->runActionWithFilters($action,$this->filters()); //запускаем экшин
				}
				else {
					//здесь начинается код отличный от кода yii, в нем мы запускаем транзакцию:
					try {
						$transaction=Yii::app()->db->beginTransaction();
						if (isset($transaction)) {
							Yii::app()->attachEventHandler('onEndRequest', array($transaction, 'commit'));
//							Yii::app()->getEventHandlers('onEndRequest')->insertAt(0,array($transaction, 'commit'));
						}
						$this->jsAppOptions['originalRoute'] = $this->getRoute($action);
						$this->runActionWithFilters($action,$this->filters()); //запускаем экшин
					} catch (Exception $e){
						if (isset($transaction)) {
							$transaction->rollback(); //всё упало
						}
						throw $e;
					}
					//продолжается всё стандарным кодом yii:
				}
				$parent->afterControllerAction($this,$action);
			}
		} else
			$this->missingAction($actionID);
	}

	/**
	 * @return array Запретим по умолчанию доступ ко всем action неавторизированным юзерам.
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'users'=>array('?'),
			),
			array('deny',
				'roles'=>array(Users::ROLE_DELETED),
			),
		);
	}

	public function filters()
	{
		return array(
			'accessControl',
			'anotherUser',
			'resourcesVersion',
			'cookieAutologinRedirect',
			'language',
		);
	}

	public function filterCookieAutologinRedirect($filterChain)
	{
		// Сессия была установлена в методе afterLogin класса WebUser
		$fromCookie = !empty(Yii::app()->session['FROM_COOKIE']);
		Yii::app()->session['FROM_COOKIE'] = null;

		if (
			!Yii::app()->request->isModal() &&
			!Yii::app()->user->isGuest &&
			!empty(Yii::app()->user->model) &&
			get_class(Yii::app()->user->model)=='Users' &&
			$fromCookie &&
			($alr = Yii::app()->userConfig->get('after_login_redirect', false)) &&
			($allow = Yii::app()->matchMask->check([
				'include' => ['mypage.general.index'],
			]))
		) {
			$this->redirect($alr, true, 302, 'redirect');
		}
		$filterChain->run();
	}

	/**
	 *  Перезагрузка страницы если id залогиненного юзера изменился
	 */
	public function filterAnotherUser($filterChain)
	{
		$idUserFrontEnd = Yii::app()->request->getIdUser();
		$idUserBackEnd = Yii::app()->user->isGuest ? null : Yii::app()->user->id;
		if (!Yii::app()->request->isModal() && !empty($idUserFrontEnd) && !empty($idUserBackEnd) && ($idUserFrontEnd != $idUserBackEnd)) {
			// set HTTP CODE to avoid ajaxError function on errors
			http_response_code(200);
			$this->redirect(Yii::app()->createUrl('/site/login'), true, 302, 'redirect');
		}
		$filterChain->run();
	}

	/**
	 *  Перезагрузка страницы если версия css/js изменилась
	 */
	public function filterResourcesVersion($filterChain)
	{
		if (Yii::app()->request->isAjaxRequest && !Yii::app()->request->isModal() && Yii::app()->request->resourcesVersion && Yii::app()->request->resourcesVersion != $this->_resourcesVersion) {
			$params = isset($_GET) ? $_GET : [];
			$this->redirect($this->createUrl('/'.$this->getRoute(), $params), true, 302, 'redirect');
		}
		$filterChain->run();
	}

	/**
	 * Фильтр контролирует чтобы юзер не ушел с текущего языка (который сохранен у него в settings)
	 */
	public function filterLanguage($filterChain)
	{
		if (
			MultilangHelper::enabled() &&
			!Yii::app()->user->isGuest &&
			!Yii::app()->request->isModal() &&
			!empty(Yii::app()->user->model) &&
			get_class(Yii::app()->user->model)=='Users' &&
			Yii::app()->getLanguage() != Yii::app()->user->model->getLanguage() &&
			$this->getRoute() != 'site/error'
		) {
			Yii::app()->setLanguage(Yii::app()->user->model->getLanguage());
			$params = isset($_GET) ? $_GET : [];
			$this->redirect($this->createUrl('/'.$this->getRoute(), $params), true, 302, 'redirect');
		}
		$filterChain->run();
	}

	public function render($view, $data=null, $return=false, $processOutput=true)
	{
		if (Yii::app()->request->isAjaxRequest) {
			Yii::app()->clientScript->registerScript('updateCsrfToken', 'appMain.appOptions.csrfTokenValue="'.CHtml::encode(Yii::app()->request->csrfToken).'"');

			if (!Yii::app()->request->isModal()) {
				if (!Yii::app()->user->getState('fromCtrl', null))
					Yii::app()->user->setOnline();
			}

			if ($return) {
				return $this->renderAjax($view, $data, $return, $processOutput);
			} else {
				$data = array(
					'content' => $this->renderAjax($view, $data, true, $processOutput),
					'container' => $this->layoutContainer,
				);

				$this->jsonResponse($data);
			}
		} else {

			$this->jsAppOptions['currentLayout'] = $this->layout;

			// состояние именованых масок кидаем на клиента (для отслеживания isChangeMask)
			if (!Yii::app()->request->isModal()) {
				$this->jsAppOptions['isMatchMasksList'] = Yii::app()->matchMask->isMatchMasksList();
			}

			if (!Yii::app()->user->getState('fromCtrl', null))
				Yii::app()->user->setOnline();

			if ($return) {
				return parent::render($view, $data, $return);
			} else {
				parent::render($view, $data, $return);
			}
		}
	}

	public function renderAjax($view,$data=null, $return, $processOutput)
	{
		if (!Yii::app()->request->isModal() && Yii::app()->request->currentLayout() && $this->layout != Yii::app()->request->currentLayout()) {
			// запрос не модальный (со сменой урла), передан текущий layout из appMain.js и он оказался другим (изменился). Следовательно нужно выполнить ajax рендер с новым layout'ом.
			$layout = $this->layout.'-ajax';
			if ($this->getLayoutFile($layout) !== false) {
				$this->layoutContainer = '#layout_container_global';
				$this->layout = $layout;
				return parent::render($view, $data, $return);
			} else {
				return parent::renderPartial($view, $data, $return, $processOutput);
			}
		} else {
			return parent::renderPartial($view, $data, $return, $processOutput);
		}
	}

	/**
	 * Редирект для обчного запроса с перезагрузкой страницы, для аякс запроса без перезагрузки
	 * @param mixed  $url
	 * @param bool   $terminate
	 * @param int    $statusCode
	 * @param string $ajaxRedirectType - если передать значение 'redirect', то аякс редирект будет тоже с перезагрузкой.
	 */
	public function redirect($url,$terminate=true,$statusCode=302, $ajaxRedirectType='soft_redirect')
	{
		if (Yii::app()->request->isAjaxRequest) {
			if(is_array($url)) {
				$route=isset($url[0]) ? $url[0] : '';
				$url=$this->createUrl($route,array_splice($url,1));
			}

			if (!empty($this->softRedirectParams)) {
				$url = CMap::mergeArray($this->softRedirectParams, array('url'=>$url));
			}

			$this->jsonResponse(array($ajaxRedirectType => $url));
		} else {
			parent::redirect($url,$terminate,$statusCode);
		}
	}

	/**
	 * @param $url string Редирект с перезагрузкой страницы в обоих случаях (аякс и обычный запрос) с последующим сообщением.
	 */
	public function expiredRedirect($url)
	{
		Yii::app()->user->setFlash('jalert-warning', array('text'=>'You are trying to access expired page.', 'title'=>'WARNING!'));
		$this->redirect($url, true, 302, 'redirect');
	}

	public function appendJsonResponse($data)
	{
		// convert callback to array
		if (!empty($this->jsonResponseData['callback']) && !is_array($this->jsonResponseData['callback'])) {
			$this->jsonResponseData['callback'] = [$this->jsonResponseData['callback']];
		}
		if (!empty($data['callback']) && !is_array($data['callback'])) {
			$data['callback'] = [$data['callback']];
		}
		$existCallback = !empty($this->jsonResponseData['callback']) ? $this->jsonResponseData['callback'] : [];
		$incomeCallback = !empty($data['callback']) ? $data['callback'] : [];

		$this->jsonResponseData = CMap::mergeArray($this->jsonResponseData, $data);
		$this->jsonResponseData['callback'] = CMap::mergeArray($existCallback, $incomeCallback);
	}

	public function jsonResponse($data)
	{
		header('Content-type: application/json');

		// normilize to array
		if (!isset($data['callback'])) {
			$data['callback'] = array();
		}
		elseif (!is_array($data['callback'])) {
			$tmp = $data['callback'];
			$data['callback'] = array($tmp);
		}

		if (!empty($this->jsonResponseData)) {
			$data = CMap::mergeArray($this->jsonResponseData, $data);
		}

		// append
		if (isset($data['callback-core'])) {
			$data['callback'][] = $data['callback-core'];
		}

		// состояние именованых масок кидаем на клиента (для отслеживания isChangeMask)
		if (!Yii::app()->request->isModal()) {
			$data['callback'][] = "appMain.setIsMathcMasksList(".CJavaScript::encode(Yii::app()->matchMask->isMatchMasksList()).")";
		}

		// ajax unload js callbacks
		if (!Yii::app()->request->isModal()) {
			foreach ($this->onAjaxUnload as $namedMask=>$callbacks) {
				if ($this->isChangeMask($namedMask) && !Yii::app()->matchMask->check($namedMask) && !empty($callbacks)) {
					foreach ($callbacks as $callback) {
						$data['callback'][] = $callback;
					}
				}
			}
		}

		$data['callback'] = implode(';', $data['callback']);

		// фильтр utf переноса строки, на котором спотыкается javascript.
		// http://stackoverflow.com/questions/14179377/how-to-replace-escape-u2028-or-u2029-characters-in-php-to-stop-my-jsonp-api-br
		$data = str_replace("\xe2\x80\xa8", '\\u2028', $data);
		$data = str_replace("\xe2\x80\xa9", '\\u2029', $data);

		// добавим в НЕмодальный ответ оригинальный route и текущий layout
		$route = $this->getRoute();
		if (!Yii::app()->request->isModal() && $route) {
			$data['originalRoute'] = $route;
			$data['currentLayout'] = mb_stristr($this->layout, '-ajax', true) ? mb_stristr($this->layout, '-ajax', true) : $this->layout;
		}

		// добавим в модальный ответ признак сохранить tinyMCE
		if (Yii::app()->request->isModal()) {
			$data['saveMce'] = 1;
		}

		if ($this->isLayoutContainerSaving) {
			$data['isLayoutContainerSaving'] = 1;
		}
		if ($this->isLayoutContainerRestoring) {
			$data['isLayoutContainerRestoring'] = 1;
		}

		echo CJavaScript::jsonEncode($data);
		Yii::app()->end();
	}

	public function jsonResponseSimple($data)
	{
		header('Content-type: application/json');
		echo CJavaScript::jsonEncode($data);
		Yii::app()->end();
	}

	public function jsonpResponse($data, $callback = null)
	{
		header('Content-type: application/javascript');
		if (!$callback) {
			$callback = Yii::app()->request->getParam('callback', null);
			if (!$callback) {
				Yii::app()->end();
			}
		}
		echo $callback . '(' . CJavaScript::jsonEncode($data) . ')';
		Yii::app()->end();
	}

	public function beforeRender($view)
	{
		if (parent::beforeRender($view)) {
			if ((!empty($_SERVER['HTTP_USER_AGENT']))&&(preg_match('/MSIE ([\d]+)/', $_SERVER['HTTP_USER_AGENT'], $matches) AND (int)$matches[1] < 10)) {
				throw new CHttpException(666, 'Your browser is an outdated piece of shit. Install modern browser and come back.');
			}

			return true;
		} else {
			return false;
		}
	}

	private function getResourcesVersion()
	{
		$versionFile = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.Yii::app()->params['resourcesVersionFilename'];
		if (file_exists($versionFile)) {
			return file_get_contents($versionFile);
		} else {
			return '';
		}
	}

	/**
	 * Хелпер быстрой регистрации яваскрипта во вьюхе
     * @param string $object имя объекта javascript
     * @param string $method имя метода из объекта
     * @param array|bool $params (optional) массив параметров
     * @param integer|null $pos - position скрипта
	 */
    public function jsInit($object, $method, $params = false, $pos = null)
	{
		$paramsStr = empty($params) ? '' : CJavaScript::encode($params);
		Yii::app()->ClientScript->registerScript(
			$object.'_'.$method.'_'.rand(111,999),
            $object . '.' . $method . "(" . $paramsStr . ")",
            $pos
        );
	}

	public function renderScrollLoadMore($model, $dataProvider, $view='index', $mergeRenderParams=array())
	{
		$data = $dataProvider->getData();
		$renderParams = array(
			'dataProvider' => $dataProvider,
			'data' => $data,
			'model' => $model,
		);
		$renderParams = CMap::mergeArray($renderParams, $mergeRenderParams);

		// догрузка постраничкой
		if (isset($_GET['loadmore']) AND $_GET['loadmore'] == 1) {
			$this->jsonResponse(array('callback' => 'appWidgetLoadMore.responseHandler('.CJavaScript::encode(array(
					'content' => $this->render('partial/'.$view.'_loop', $renderParams, true),
					'nextPageUrl' => $dataProvider->pagination->nextPageUrl,
				)).')'));
		}
		// обычное отображение страницы
		else {
			$this->render($view, $renderParams);
		}
	}

	public function registerGoogleAnalytics()
	{
		$code =<<<EOT
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-737594-16', 'udimi.com');
  ga('send', 'pageview');
EOT;
		if (!YII_DEBUG) {
			Yii::app()->clientScript->registerScript('global-google-analytics', $code, CClientScript::POS_HEAD);
		}
	}

	public function getRoute($action=false)
	{
		if ($action===false && $this->action === null) {
			return false;
		}
		$actionId = $action ? $action->id : $this->action->id;
		$route = $this->module ? $this->module->id : null;
		$route = $route ? $route."/".$this->id : $this->id;
		return $route."/".$actionId;
	}

	public function isChangeMask($name)
	{
		if (!Yii::app()->request->isModal() && Yii::app()->request->getIsMatchMasksList()) {

			$isMatchMask = Yii::app()->matchMask->check($name) ? 1 : 0;

			$isMatchMasksList = CJSON::decode(Yii::app()->request->getIsMatchMasksList());
			if ($isMatchMasksList && isset($isMatchMasksList[$name])) {
				return ($isMatchMasksList[$name]!=$isMatchMask);
			} else {
				return false;
			}

		} else {
			return false;
		}
	}

	public function fbOgTags()
	{
		Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl('/'), null, null, array('property'=>'og:url'));
		Yii::app()->clientScript->registerMetaTag('website', null, null, array('property'=>'og:type'));
		Yii::app()->clientScript->registerMetaTag('Udimi - Buy email solo ads', null, null, array('property'=>'og:title'));
		Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl('/').$this->baseUrlImg.'/Udimi-buy-solo-fblogo.jpg', null, null, array('property'=>'og:image'));
		Yii::app()->clientScript->registerMetaTag('Buy Email Solo Ads', null, null, array('property'=>'og:description'));
	}

	public function package($pos=CClientScript::POS_HEAD, $options=[])
	{
		$name = $this->layoutPackageName.(YII_DEBUG ? '' : '.min');
		$v = empty($this->_resourcesVersion) ? '1' : $this->_resourcesVersion;
		Yii::app()->clientScript->registerCssFile('/media/css'.(YII_DEBUG ? '-dev' : '').'/'.$name.'.css?v='.$v);
		Yii::app()->clientScript->registerScriptFile('/media/js'.(YII_DEBUG ? '-dev' : '').'/'.$name.'.js?v='.$v, $pos, $options); //POS_HEAD, ['defer'=>'defer']
	}

	protected function crossHeaders()
	{
		// answer cross domain ajax request
		if (!empty($_SERVER['HTTP_ORIGIN'])) {
			header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
			header('Access-Control-Allow-Methods: POST, GET');
			header('Access-Control-Max-Age: 1000');
			header('Access-Control-Allow-Headers: Content-Type');
		}
	}

	public function createAbsoluteUrlSch($route, $params=[])
	{
		$url = Yii::app()->createAbsoluteUrl($route, $params, 'http');
		return str_replace('http:', '', $url);
	}

	public function createUrl($route,$params=array(),$ampersand='&')
	{
		if (isset($params['noautologin'])) {
			$noautologin = $params['noautologin'];
		} elseif (isset($_GET['noautologin'])) {
			$noautologin = $_GET['noautologin'];
		}
		if (isset($noautologin) && $noautologin) {
			$params['noautologin'] = 1;
		}
		return parent::createUrl($route,$params,$ampersand);
	}

	public function renderError($arg)
	{
		return $this->render('application.views.default.error', $arg, true);
	}
}