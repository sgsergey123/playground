<?php

class SiteController extends Controller
{
	public function filters()
	{
		return CMap::mergeArray(parent::filters(), array(
				'authorizedRedirect + login, forgot, index, signup',
			));
	}

	public function filterAuthorizedRedirect($filterChain)
	{
		$noautologin = isset($_GET['noautologin']) && $_GET['noautologin'] == 1 ? 1 : 0;
		if (!Yii::app()->user->isGuest && !$noautologin) {
			$this->redirect(array(Yii::app()->user->returnUrl));
		}
		$filterChain->run();
	}

	public function actionIndex()
	{
		$this->redirect(['login']);
	}

	public function actionSignup()
	{
		$model = new Users('register');

		if (isset($_POST['Users'])) {
			$model->attributes = $_POST['Users'];

			// trying to autologin
			$lf = new LoginForm();
			$lf->username = $model->email;
			$lf->password = $model->password;
			$lf->fpToken = $model->fpToken;
			$lf->fpToken2 = $model->fpToken2;
			$lf->skipLogging = true;

			if ($lf->validate() && $lf->login()) {
				$this->redirect(Yii::app()->user->returnUrl, true, 302, 'redirect');
			}

			if ($model->validate()) {
				$model->makeUser();

				$identity = new UserIdentity('', '');
				$identity->setId($model->id);
				$identity->setState('fpToken', $model->fpToken);
				$identity->setState('fpToken2', $model->fpToken2);
				$identity->saveLoginToken($model);
				Yii::app()->user->allowAutoLogin = true;
				Yii::app()->user->login($identity, 3600*24*UserIdentityKeys::COOKIE_LOGIN_TTL);

				$defaultUrl = $this->createUrl('/settings/general/index');
				$this->redirect($defaultUrl, true, 302, 'redirect');
			}
		}

		$this->render('signup', ['model'=>$model]);
	}

	public function actionLogin()
	{
		$model = new LoginForm;
		if (!empty(Yii::app()->session["LoginForm_username"]))
			$model->username = Yii::app()->session["LoginForm_username"];

		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			Yii::app()->session["LoginForm_username"] = $model->username;

			if ($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl, true, 302, 'redirect');
			}
		}

		$this->render('login', ['model' => $model]);
	}

	public function actionForgot()
	{
		$modelName = 'ForgotForm';
		$model = new ForgotForm();
		if (!empty(Yii::app()->session["LoginForm_username"]))
			$model->username = Yii::app()->session["LoginForm_username"];

		if (isset($_POST[$modelName])) {
			$model->attributes = $_POST[$modelName];

			if ($model->validate()) {
				$user = Users::model()->findByAttributes(array('email' => $model->username));

				$key = UserIdentityKeys::create($user, 'forgot_password');

				// send email


				$this->jsonResponse(array(
					'callback' => 'appMain.showToast("Link to reset your password has been sent to your email", "success")',
					'soft_redirect' => $this->createUrl('/site/login'),
				));
			}
		}

		$this->render('forgot', array('model' => $model));
	}

	public function actionRestore($key)
	{
		if (!Yii::app()->user->isGuest) {
			Yii::app()->user->logout(false);
		}

		$identity = new UserIdentityByKey($key, 'forgot_password');
		$identity->authenticate();

		if ($identity->errorCode === UserIdentityByKey::ERROR_NONE) {

			$model = Users::model()->findByPk($identity->id);
			$model->setScenario('restore');

			if (!empty($_POST['Users'])) {
				$model->attributes = $_POST['Users'];
				if ($model->validate()) {
					$model->save();

					$identity->setState('fpToken', $model->fpToken);
					$identity->setState('fpToken2', $model->fpToken2);
					Yii::app()->user->login($identity, 3600*24*UserIdentityKeys::COOKIE_LOGIN_TTL);

					Yii::app()->user->setFlash('jalert-warning', array(
							'text' => 'Password has been successfully changed',
							'title' => 'Complete'
						));

					$this->redirect(array(Yii::app()->user->returnUrl), true, 302, 'redirect');
				}
			}

			$this->render('restore', array('model' => $model));

		} elseif ($identity->errorCode === UserIdentityByKey::ERROR_USER_NOT_FOUND) {
			throw new CHttpException(404, 'Page not found');

		} else {
			$this->skipTopMenu = true;
			$this->render('restore_expired');
		}
	}

	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect(Yii::app()->createUrl(Yii::app()->user->loginUrl));
	}

	public function actionError()
	{
		if ($error=Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo '['.$error['code'].'] '.$error['message'];
			} else {
				$this->renderPartial('error', array('data'=>$error));
			}
		}
	}

	public function action404()
	{
		throw new CHttpException(404, 'Page not found');
	}

	public function accessRules()
	{
		return array();
	}
}