<div class="sidebar-wrapper width-sidebar messages-fixed-panel">
	<div id="app-layout-float-sidebar">
		<?php if (Yii::app()->user->isGuest): ?>
			<div class="btn-login">
				<a href="<?= $this->createUrl('/site/index') ?>" class="btn btn-primary">Register</a>
			</div>
		<?php else:
			$this->widget('w.wMenuLeft.wMenuLeft');
		endif ?>
	</div>
</div>

<div class="content-wrapper width-sidebar">
	<div class="float layout-content" id="layout_content" data-pagecontainer="true">
		<?= $content ?>
	</div>
	<div class="clearfix"></div>
</div>

<div class="clearfix"></div>