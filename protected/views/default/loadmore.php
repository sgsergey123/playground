<?php $this->widget('w.wPager.wPager', array(
	'pages' => $dataProvider->pagination,
	'mode' => 'load-more-scroll',
	'loadMoreLabel' => false,
)) ?>
<div class="search-results">
	<?php if ($data):

		$alias = '';
		$alias .= 'application';
		if (isset($this->module->id)) {
			$alias .= '.modules.'.$this->module->id;
		}
		$alias .= '.views.'.$this->id.'.partial';

		if (isset($viewLoop)) {
			$file = $viewLoop;
		} else {
			$file = $this->action->id.'_loop.php';
		}

		include(Yii::getPathOfAlias($alias).DIRECTORY_SEPARATOR.$file);

		echo CHtml::tag('div', array('id' => 'load_more_container'), '');
	else:
		echo CHtml::tag('div', array('class' => 'no-items'), empty($empty_message) ? 'No results' : $empty_message);
	endif;?>
</div>