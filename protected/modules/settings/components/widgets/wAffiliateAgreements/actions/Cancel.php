<?php
class Cancel extends WidgetBaseAction
{
	public function run($uid)
	{
		if (!$model = AffiliateAgreements::model()->findByAttributes(['uid'=>$uid, 'id_seller'=>Yii::app()->user->id]))
			throw new CHttpException(404, 'Agreement not found');

		// Бизнес логика спрятана в метод. В экшне ее не должно быть, потому что
		// существует еще один экшн, который делает тоже самое (в income proposals list модуля solos).
		$model->makeCancel();

		$widget = $this->getWidgetInstance();
		$this->controller->jsonResponse([
			'callback'=>[
				'settingsWidgetAgreements.init()',
			],
			'container' => '#'.$widget->containerId,
			'content' => $widget->run(),
		]);
	}
}