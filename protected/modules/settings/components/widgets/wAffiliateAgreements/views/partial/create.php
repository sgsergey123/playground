<?php $this->jsInit('settingsWidgetAgreements', 'initCreate'); ?>

<?php $form = $this->beginWidget('CActiveForm', [
	'htmlOptions' => [
		'data-init' => 'dirty-check',
		'class' => 'form-horizontal',
	],
]); ?>

	<div class="b-row">
		<div class="b-col-1">
			<?= $form->labelEx($model, 'profile_link'); ?>
		</div>
		<div class="b-col-2">
			<?= $form->textField($model, 'profile_link', ['class'=>'form-control js-profile-link', 'data-href'=>$this->createUrl('waffiliateagreements.update')]) ?>
		</div>
	</div>

	<div class="b-row">
		<div class="b-col-1">
			<?= $form->labelEx($model, 'percent'); ?>
		</div>
		<div class="b-col-2">
			<?= $form->dropDownList($model, 'percent', $model->getPercentOptions(), ['class'=>'form-control js-percent', 'data-init'=>'chosen-list', 'data-width'=>'86px', 'data-href'=>$this->createUrl('waffiliateagreements.update')]) ?>
		</div>
	</div>

	<div class="b-row agreements-list preview-container">
		<?php $this->renderPartial('application.modules.settings.components.widgets.wAffiliateAgreements.views.partial.partner_item', ['user'=>$model->profileUserModel, 'percent'=>$model->percent]);?>
	</div>

	<div class="e-sbt">
		<button type="submit" data-href="<?=$this->createUrl('waffiliateagreements.create');?>" data-widget="waffiliateagreements" class="btn btn-primary ajax-post">Save</button>
	</div>

	<div class="e-cancel">
		<span data-href="<?=$this->createUrl('waffiliateagreements.reload')?>" data-widget="waffiliateagreements" class="ajax-get-modal">cancel</span>
	</div>

	<div class="clearfix"></div>
<?php $this->endWidget() ?>
