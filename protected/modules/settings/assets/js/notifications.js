function SettingsNotifications() {

	this.options = {}

	var self = this;

	this.init = function()
	{
		$('.email-types-checkbox').bind('change', function() {
			$(this).parents('form').find('.js-email-types-submit').click();
		});

	}
}
var settingsNotifications = new SettingsNotifications();