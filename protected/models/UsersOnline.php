<?php
class UsersOnline extends ActiveRecord
{
	public $old_is_online;

	public function tableName()
	{
		return 'users_online';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
	{
		if (parent::beforeSave()) {

			if ($this->isNewRecord) {
				if ($this->is_online === null) {
					$this->is_online = 1;
				}
				if ($this->dta_login === null) {
					$this->dta_login = date('Y-m-d H:i:s');
				}
                $this->setIP();
			}
            if (!$this->language) {
                $this->detectLanguage();
            }

			return true;
		} else {
			return false;
		}
	}

    public function setIP()
    {
        if ((\Yii::app()->hasComponent('session') && \Yii::app()->session->get('fromCtrl')) || $this->idUser->role == Users::ROLE_POWER) {
            return;
        }
        if (!empty($_SERVER['REMOTE_ADDR'])) {
            $this->ip = $_SERVER['REMOTE_ADDR'];
            $this->ip_aton = ip2long($_SERVER['REMOTE_ADDR']);
        }
        if (!empty($this->ip) && empty($this->is_custom_location)) {
            $ipData = Ip2Location::selectInfoLocation($this->ip);
            if (!empty($ipData['ipLATITUDE'])) {
                $this->lat = $ipData['ipLATITUDE'];
            }
            if (!empty($ipData['ipLONGITUDE'])) {
                $this->lng = $ipData['ipLONGITUDE'];
            }
        }
    }

    public function detectLanguage()
    {
        if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            $langs = explode(',', $lang);
            $pref = $langs[0];
            if (strpos($pref, ';') !== false) {
                $pref = explode(';', $pref)[0];
            }
            if (strpos($pref, '-') !== false) {
                $pref = explode('-', $pref)[0];
            }
            if ($pref) {
                $this->language = $pref;
            }
        }
    }

	public static function getCntOnline()
	{
		return self::model()->count('is_online=1');
	}

	public function relations()
	{
		return array(
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
			'rc' => array(self::HAS_ONE, 'RatchetConnections', ['id_user'=>'id_user']),
		);
	}

	protected function afterFind()
	{
		parent::afterFind();

		$this->old_is_online = $this->is_online;
	}
}
