<?php
class EmailLog extends ActiveRecord
{
	public function tableName()
	{
		return 'email_log';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'email' => 'Email',
			'dta' => 'Dta',
			'body' => 'Body',
			'params' => 'Params',
			'template' => 'Template',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
