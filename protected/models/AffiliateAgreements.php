<?php

class AffiliateAgreements extends ActiveRecord
{
	public $profile_link = null;
	public $profileUserModel;

	public function tableName()
	{
		return 'affiliate_agreements';
	}

	public function rules()
	{
		return [
			['percent, profile_link', 'required', 'on'=>'create-save'],
			['percent', 'validatePercent', 'on'=>'create-save, create-update, edit'],
			['profile_link', 'validateProfileLink', 'on'=>'create-save, create-update'],
		];
	}

	public function validatePercent($attribute, $params)
	{
		$percents = AffiliateAgreements::getPercentOptions();
		$keys = array_keys($percents);
		if (!in_array($this->$attribute, $keys)) {
			$this->addError($attribute, 'Field "'.$this->getAttributeLabel($attribute).'" has wrong value.');
			if ($this->scenario=='create-update') {
				$this->percent = $keys[0];
			}
		}
	}

	public function validateProfileLink($attribute, $params)
	{
		$uid = str_replace(['http://','https://',Yii::app()->params['system_domain'].'/'], '', trim($this->$attribute));

		if (
			!preg_match('/^[a-z0-9_]{5,32}+$/i', $uid)
			|| in_array($uid, Users::notAllowedProfileNames())
			|| !$this->profileUserModel=Users::model()->findByAttributes(['uid_profile'=>$uid])
		) {
			$this->addError($attribute, Yii::t('app', 'Profile does not exist'));
			if ($this->scenario=='create-update') {
				$this->profileUserModel = null;
			}

		} elseif ($uid == Yii::app()->user->model->uid_profile) {
			if ($this->scenario=='create-save') {
				$this->addError($attribute, Yii::t('app', 'You cannot create an agreement with yourself'));
			}

		} elseif (AffiliateAgreements::model()->findByAttributes(['id_seller'=>Yii::app()->user->id, 'id_partner'=>$this->profileUserModel->id])) {
			if ($this->scenario=='create-save') {
				$this->addError($attribute, Yii::t('app', 'You already have an agreement with this member'));
			}
		}
	}

	public function relations()
	{
		return array(
			'idSeller' => array(self::BELONGS_TO, 'Users', 'id_seller'),
			'idPartner' => array(self::BELONGS_TO, 'Users', 'id_partner'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'profile_link'=>'Partner\'s profile link',
			'percent'=>'Affiliate commission',
		);
	}

	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				$this->dta_create = new CDbExpression('NOW()');
				if (empty($this->uid)) {
					$this->uid = MyUtils::generateUniqueStringUid(16, $this->tableName(), 'uid');
				}
			}
			$this->dta_update = new CDbExpression('NOW()');
			return true;
		} else {
			return false;
		}
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getBaseCriteria($id_user)
	{
		$criteria = new CDbCriteria();
		$criteria->with = ['idPartner'];
		$criteria->addCondition('t.id_seller=:id_user');
		$criteria->params = [':id_user'=>$id_user];
		$criteria->order = 't.id DESC';
		return $criteria;
	}

	public function search($id_user)
	{
		return new CActiveDataProvider($this, array(
			'criteria'=>$this->getBaseCriteria($id_user),
			'pagination' => array(
				'class' => 'LoadMorePagination',
				'reverse' => false,
				'pageSize' => 25,
			),
		));
	}

	public static function getPercentOptions($skipFirst=true)
	{
		$options = [];
		$val = $skipFirst ? 1 : 0;
		for ($i=round(0.15*100)+$val; $i<=100; $i++) $options[$i] = $i.'%';
		return $options;
	}

	public function createUrl()
	{
		return Yii::app()->createAbsoluteUrl('/profile/general/index', ['sellerUid'=>$this->idSeller->uid, 'kentUid'=>$this->idPartner->uid], Yii::app()->params['id_local'] ? 'http' : 'https');
	}

	public function makeAgr($id_seller, $id_partner)
	{
		$this->id_seller = $id_seller;
		$this->id_partner = $id_partner;
		$this->save(false);

		Mailer::mail($this->idPartner, 'aff_agreements_created', [
			'data'=>[
				'percent' => $this->percent,
				'url' => $this->createUrl(),
				'idSeller'=>$this->idSeller,
			]
		]);
	}

	public function makeCancel()
	{
		$this->delete();

		Mailer::mail($this->idPartner, 'aff_agreements_cancelled', [
			'data'=>[
				'percent' => $this->percent,
				'idSeller'=>$this->idSeller,
			]
		]);
	}
}
