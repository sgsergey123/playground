<?php
class ForgotForm extends CFormModel
{
	public $username;

	public function rules()
	{
		return array(
			array('username', 'HackStringValidate'),
			array('username', 'required', 'message'=>'Email is required'),
			array('username', 'email', 'message'=>'Email invalid'),
			array('username', 'exist', 'attributeName' => 'email', 'className' => 'Users'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'username' => 'Email',
		);
	}
}